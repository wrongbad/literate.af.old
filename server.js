const fs = require('fs')

const whoiskf = require('whoiskf.js')
const literate = require('literate.js')


const whokf_ssl = {
    key: '/etc/letsencrypt/live/whoiskylefinn.com/privkey.pem',
    cert: '/etc/letsencrypt/live/whoiskylefinn.com/fullchain.pem'
}

const litaf_ssl = {
    key: '/etc/letsencrypt/live/literate.af/privkey.pem',
    cert: '/etc/letsencrypt/live/literate.af/fullchain.pem'
}

const enable_proxy = 
    fs.existsSync(whokf_ssl.key) && 
    fs.existsSync(whokf_ssl.cert) && 
    fs.existsSync(litaf_ssl.key) && 
    fs.existsSync(litaf_ssl.cert);

console.log("Starting server with proxy: "+enable_proxy);

// disabled for localhost development
if(enable_proxy)
{
    const redbird = require('redbird')

    const proxy = redbird({
        port: 8080,
        ssl: {
            port: 8443,
            redirectPort: 443,
        },
    });

    proxy.register('whoiskylefinn.com', 'localhost:8002', {
        ssl: whokf_ssl,
    });

    proxy.register('literate.af', 'localhost:8001', {
        ssl: litaf_ssl,
    });

    proxy.register('www.whoiskylefinn.com', 'localhost:8002', {
        ssl: whokf_ssl,
    });

    proxy.register('www.literate.af', 'localhost:8001', {
        ssl: litaf_ssl,
    });


    // this thing is totally independent
    const gossl = redbird({
      port: 1368, // unused
      ssl: {
        port: 1369,
      },
    });

    gossl.register('whoiskylefinn.com', 'localhost:9369', {
        ssl: whokf_ssl,
    });
}
