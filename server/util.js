

function isstr(s) { return (typeof s)=='string'||(s instanceof String); }
function safestr(s) { return isstr(s) ? s.replace(/[^0-9A-Za-z\-_]/g,"").substr(0,20) : ""; }
function tojson(o) { try { return JSON.stringify(o); } catch(err) { console.log('tojson::'+err+' '+o); return null; } }
function fromjson(o) { try { return JSON.parse(o); } catch(err) { console.log('fromjson::'+err+' '+o); return null; } }

function issafestr(s)
{ 
    var char = function(s) { return s.charCodeAt(0); }
    if(!isstr(s) || s.length > 32 || s.length == 0)
        return false;
    for(var i=0 ; i<s.length ; i++)
    {
        var c = s.charCodeAt(i);
        if(c >= char('A') && c <= char('Z'))
            continue;
        if(c >= char('a') && c <= char('z'))
            continue;
        if(c >= char('0') && c <= char('9'))
            continue;
        if(c == char('-') || c == char('_'))
            continue;
        return false;
    }
    return true;
}

exports.safestr = safestr;
exports.issafestr = issafestr;
exports.fromjson = fromjson;
exports.tojson = tojson;