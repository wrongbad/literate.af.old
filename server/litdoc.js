const fs = require('fs')
const path = require('path')
const nodeutil = require('util')
// const ncp = require('ncp')

// ncp.limit = 16;
const copyDir = nodeutil.promisify(require('ncp'));


const readDir = nodeutil.promisify(fs.readdir);
// const parseForm = nodeutil.promisify(form.parse);
// const fileExists = nodeutil.promisify(fs.access);
const makeDir = nodeutil.promisify(fs.mkdir);
const readFile = nodeutil.promisify(fs.readFile);
const writeFile = nodeutil.promisify(fs.writeFile);
const deleteFile = nodeutil.promisify(fs.unlink);


const litparse = require('../public/litter/litparse.js')


const _mkdirp = nodeutil.promisify(require('mkdirp'));

const mkdirp = async function(path)
{   try 
    {   await _mkdirp(path);
    }
    catch(e) 
    {   // because some idiot thought 'mkdir -p' should throw an error if already exists
        // despite the opposite behavior being the whole point of 'mkdir -p'
        if(e.code != 'EEXIST')
        {   throw e;
        }
    }
}

// var mutex = require( 'node-mutex' )();

// var mutexLock = nodeutil.promisify(mutex.lock);

// mutex.lock('key', function(err, unlock)
// {   if(err)
//     {   throw err;
//     }

//     unlock();
// }


// import {Client, Broker} from 'live-mutex';
// const mutex = require('live-mutex');
// // const mutUtils = require('live-mutex/utils');
// const mutOpts = {port: 1034 , host: '127.0.0.1'};
// const mutClient = new mutex.Client(mutOpts);

// mutex.utils.conditionallyLaunchSocketServer(mutOpts, function(err)
// {   if(err)
//     {   throw err; 
//     }
// });

// function mutexLock(key)
// {   return new Promise((resolve) => 
//     {   mutClient.ensure((err) =>
//         {   if(err) throw err;
//             mutClient.lock(key, (err, unlock) => 
//             {   if(err) throw err;
//                 resolve(unlock);
//             });
//         });
//     });
// }


// tried 3 different mutex packages. 
// 0 worked out of the box with the given example code.
// so here's an await compatible mutex for a single-thread event loop

var lockHolders = {};
var lockWaiters = {};

function mutexLock(key)
{   if(!lockHolders[key])
    {   let id = Math.random();
        lockHolders[key] = id;
        return new Promise((resolve) =>
        {   resolve( () => _mutexUnlock(key, id) );
        });
    }
    else
    {   return new Promise((resolve) =>
        {   if(!lockWaiters[key])
            {   lockWaiters[key] = [];
            }
            lockWaiters[key].push(resolve);
        });
    }
}

function _mutexUnlock(key, id)
{   if(lockHolders[key] != id)
    {   throw 'fraud mutex unlock'
    }
    if(lockWaiters[key] && lockWaiters[key][0])
    {   let next = lockWaiters[key][0];
        if(lockWaiters[key].length == 1)
        {   delete lockWaiters[key];
        }
        else
        {   lockWaiters[key].splice(0,1);   
        }
        let id = Math.random();
        lockHolders[key] = id;
        setImmediate(() => 
        {   next( () => _mutexUnlock(key, id) );
        });
    }
    else
    {   delete lockHolders[key];
    }
}



function projectPath(p)
{
    var path = "litterbox/"+p.username;
    if(p.project) path += "/"+p.project;
    if(p.version) path += "@"+p.version;
    return path;
}


async function generateApi(filedata, p)
{
    let filename = p.username;
    if(p.project) filename += '.'+p.project;
    if(p.file) filename += '.'+p.file;

    let path = "public/modules/"+filename+".js";
    let apiconcat = ""; 
    let requirecat = "";
    let fileobj = JSON.parse(filedata);
    if(!fileobj || !fileobj.cells)
        throw "bad filedata";

    for(let cell of fileobj.cells)
    {   // console.log(cell);
        let dirs = litparse.getDirectives(cell.text);

        if(cell.type=="js" && (cell.scope=="api" || dirs.includes('api')))
            apiconcat += cell.text + ";\n";

        if(cell.type=="req" && cell.scope=="api")
            requirecat += cell.text + "\n";
    }

    // generate a module even if no api
    let requireLibs = [];
    let requireVars = [];
    let lines = requirecat.split("\n");
    for(let l of lines) if(l)
    {   // includes quotes in libname
        let pair = /([\w-]+).*=.*("[\w-\/]+")/.exec(l);
        if (pair && pair[1] && pair[2])
        {   requireVars.push(pair[1]);
            requireLibs.push(pair[2]);
        }
    }

    let dirs = litparse.getDirectives(apiconcat);
    litparse.execDirectives(dirs,
    {   importAs: (lib, name)=>
        {   requireLibs.push(`"${lib}"`); 
            requireVars.push(name); 
        }
    });

    let output = 
`define(
    "${filename}",
    [ ${requireLibs.join(",")} ],
    function( ${requireVars.join(",")} ) {
        $exports = {};
// --- begin docuemnt api code ---

${apiconcat}
// --- end document api code ---
        return $exports;
    }
);`;

    await writeFile(path, output);
}
// exports.generateApi = generateApi;

function renderModule(filename, requireLibs, requireVars, code)
{
    return `define(
    "${filename}",
    [ ${requireLibs.join(",")} ],
    function( ${requireVars.join(",")} ) {
        $exports = {};
// --- begin docuemnt api code ---

${code}
// --- end document api code ---
        return $exports;
    }
);`;
}

exports.makeApi = async function(litpath, filepath, apipath)
{
    let js = null;

    let mutexUnlock = await mutexLock(filepath);
    
    // console.log(`api ${JSON.stringify(litpath)} ${filepath} ${apipath}`);

    try 
    {   if(fs.existsSync(filepath))
        {   let modname = litparse.moduleName(litpath);
            let docfile = await readFile(filepath);
            let doc = JSON.parse(docfile);
            litparse.modernize(doc);

            let requireLibs = [];
            let requireVars = [];
            let code = '';

            for(let cell of doc.cells)
            {   let dirs = litparse.getDirectives(cell.text);
                litparse.execDirectives(dirs,
                {   importAs: (lib, name) =>
                    {   requireLibs.push(`"${lib}"`); 
                        requireVars.push(name); 
                    },
                    api: () =>
                    {   code += cell.text + ';\n'
                    }
                });
            }

            js = renderModule(modname, requireLibs, requireVars, code);
            
            if(js)
            {   await mkdirp(path.dirname(apipath));
                await writeFile(apipath, js);
            }
        }
    }
    catch(e) 
    {   console.log(e);
    }

    mutexUnlock();

    return js;
}


exports.putDoc = async function(filedata, filepath, apipath)
{   let status = 500;

    let mutexUnlock = await mutexLock(filepath);
    
    try 
    {   await mkdirp(path.dirname(filepath));
        let conflict = false;
        let same = false;
        if(fs.existsSync(filepath))
        {   let prevFile = await readFile(filepath);
            let newDoc = JSON.parse(filedata);
            let prevDoc = JSON.parse(prevFile);
            conflict = (newDoc.prevHash != prevDoc.hash);
            same = (newDoc.hash == prevDoc.hash);
            if(conflict)
            {   console.log(`doc prevHash: ${newDoc.prevHash} prevDoc hash: ${prevDoc.hash}`);
            }
        }
        if(conflict)
        {   status = 409;
        }
        else
        {   await writeFile(filepath, filedata);   
            if(fs.existsSync(apipath)) //&& !same)
            {   await deleteFile(apipath);
            }
            status = 200;
        }
    }
    catch(e) 
    {   console.log(e);
    }

    mutexUnlock();

    return status;
}


exports.getMeta = async function(litpath, filepath)
{   let meta = {};
    meta.contained = [];

    try
    {   if(!litpath.file)
        {   let contentpath = filepath;
            if(litpath.project) contentpath += '/f/'
            else contentpath += '/p/';
            if(fs.existsSync(contentpath))
            {   let items = await readDir(contentpath);
                for(let i=0 ; i<items.length ; i++)
                {   if(litparse.validProject(items[i]))
                    {   meta.contained.push(items[i]);
                    }
                }
            }
            if(litpath.project)
            {   let verspath = filepath+'/v/';
                if(fs.existsSync(verspath))
                {   let items = await readDir(verspath);
                    let maxVersInt = -1;
                    let maxVers = null;
                    for(let i=0 ; i<items.length ; i++)
                    {   let vers = litparse.parseVersion(items[i]);
                        if(vers && vers.versionInt > maxVersInt)
                        {   maxVersInt = vers.versionInt
                            maxVers = vers.version;
                        }
                    }
                    meta.maxVersion = maxVers || '0.0.0';
                }
            }
        }
    }
    catch(e)
    {   console.log(e);
        return {error: 'unknown'}
    }

    return {meta: meta};
}


exports.publishVersion = async function(projpath, verspath)
{   try
    {   let projpathf = projpath + '/f/';
        let verspathf = verspath + '/f/';
        if(!fs.existsSync(projpathf))
        {   return 400;
        }
        if(fs.existsSync(verspathf))
        {   return 409;
        }
        await mkdirp(path.dirname(verspathf));
        await copyDir(projpathf, verspathf);
        if(fs.existsSync(projpath+'/index'))
        {   await copyDir(projpath+'/index', verspath+'/index');
        }
    }
    catch(e)
    {   console.log(e);
        return 500;
    }
    return 200
}
