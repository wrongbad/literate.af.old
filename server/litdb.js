
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId; 


const dbUrl = 'mongodb://localhost:27017';

const dbName = 'lit';

const client = MongoClient(dbUrl);

//{useNewUrlParser: true}
let db = null;
async function getDb()
{   if(!db)
    {   await client.connect();
        db = client.db(dbName);
    }
    return db; 
}


function genCode()
{   const chars = '!@#$%^&*()-=_+?/>.<,:;4444';
    let code = '';
    for(let i=0 ; i<14 ; i++)
    {   // TODO - secure random
        code += chars[Math.random()*chars.length|0];
    }
    return '!'+code+'!';
}
// function Invite(fromUser)
// {   return {
//         code: genCode(),
//         fromUser: fromUser || null,
//         redeemDate: null,
//         redeemUser: null
//     };
// }

exports.createInvite = async function(fromUser, memo)
{   let db = await getDb();
    let invites = db.collection('invites');
    let invProto = {
        code: genCode(),
        fromUser: fromUser ? fromUser._id : null,
        redeemDate: null,
        redeemUser: null,
        memo: memo || '',
    };
    let result = await invites.updateOne(
        { code: genCode() },
        {   $setOnInsert: invProto,
            $currentDate: { createDate:true }
        },
        { upsert: true }
    );
    // console.log("createInvite "+result);
    return result.upsertedId ? invProto : null;
}

async function createUser(username, passhash, inviteBy)
{   let db = await getDb();
    let users = db.collection('users');
    let result = await users.insertOne(
        { username:username, passhash:passhash, invitedBy:inviteBy, canInvite:(!inviteBy) }
    );
    console.log("createUser "+result);
    return result.ops ? result.ops[0] : null;
}

async function startup()
{
    let db = await getDb();
    
    let users = db.collection('users');
    await users.createIndex({ name: 1 }, { unique:true, sparse:true });

    let invites = db.collection('invites');
    await invites.createIndex({ code: 1 }, { unique:true, sparse:true });

    let invitesUsed = db.collection('inviteClaims');
    await invitesUsed.createIndex({ code: 1 }, { unique:true, sparse:true });

    let openInvites = await invites.find({ fromUser:null, redeemUser:null });
    openInvites = await openInvites.toArray();

    if(openInvites.length == 0)
    {
        let newInvite = await exports.createInvite();
        console.log("invite code: "+newInvite.code);
    }
    else for(let inv of openInvites)
    {
        console.log("invite code: "+inv.code);
    }
}


startup();

// root-user:
// id
// name

// user:
// id
// name
// passhash
// invited-by-id

// invite:
// id
// code
// from-user (or null)
// create-date
// redeem-user (or null)
// redeem-date
// expire-date (or null)

// invite-claims:
// code
// username

async function claimInviteByCode(inviteCode, forUsername)
{   let db = await getDb();
    let invites = db.collection('invites');
    let invitesClaimed = db.collection('inviteClaims');
    let inv = await invites.findOne({ code:inviteCode });
    console.log('inv = '+JSON.stringify(inv));
    if(inv)
    {   try 
        {   let claim = await invitesClaimed.insertOne({ code:inviteCode, username:forUsername });
            if(claim.insertedId)
            {   await invites.updateOne({ code:inviteCode }, 
                {   $set:{ redeemUser:forUsername },
                    $currentDate:{ redeemDate:true }
                });
                return inv;
            } 
        }
        catch(e)
        {   console.log(e);
        }
    }
    return null;
}
exports.getUserById = async function(userId)
{   let db = await getDb();
    let users = db.collection('users');
    let user = await users.findOne({ _id:ObjectId(userId) });
    // console.log('getuser '+JSON.stringify(user));
    return user;
}
exports.getUserByName = async function(username)
{   let db = await getDb();
    let users = db.collection('users');
    let user = await users.findOne({ username:username });
    return user;
}
exports.getUserInvites = async function(fromUser)
{   let db = await getDb();
    let invites = db.collection('invites');
    invites = await invites.find({ fromUser:ObjectId(fromUser._id) }).toArray();
    return invites;
}


exports.registerUser = async function(username, passhash, inviteCode)
{   let existingUser = await exports.getUserByName(username);
    if(existingUser)
    {   console.log('already '+existingUser);
        return { error:{ status:409, msg:"username taken" } };
    }
    let invite = await claimInviteByCode(inviteCode, username);
    if(invite)
    {
        try {
            let u = await createUser(username, passhash, invite.fromUser);
            return { user:u };
        }
        catch(e) {
            return { error:{ status:500, msg:"could not create user" } };
        }
    }
    return { error:{ status:409, msg:"invite already used" } };
}
