const fs = require('fs')
const url = require('url')
const path = require('path')
const helmet = require('helmet')
const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const multiparty = require('multiparty')
const nodeutil = require('util')

const auth = require('auth.js')
const util = require('util.js')
const litdoc = require('litdoc.js')

const litparse = require('../public/litter/litparse.js')







// route middleware to make sure a user is logged in
function apiRestrict(request, response, next) 
{   if (request.user)
    {   return next();
    }
    response.sendStatus(401);
}


const pubpath = path.resolve(__dirname+'/../public');

const app = express();

app.get("/", function(req, res) {
    res.sendFile("/litter/index.html", {root: pubpath} )
});
app.use(express.static('public'));

// add utility middleway to parse fancy requests
app.use(helmet({noSniff: false})); 

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    secret: fs.readFileSync("/etc/letsencrypt/live/whoiskylefinn.com/cookie-secret.txt", "utf8"),
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));

// add auth support
auth.addToApp(app);


async function parseForm(request)
{   var form = new multiparty.Form();
    return new Promise(function(resolve, reject)
    {   form.parse(request, function(err, fields, files)
        {   if(err) return reject();
            resolve(fields);
        });
    });
}


async function getDocHandler(request, response)
{
    // handle version migrations this way?
    console.log("doc 404!")
    return response.sendStatus(404);
}
async function putDocHandler(request, response)
{   let docpath = litparse.docPath(request.litpath);
    docpath = path.join(pubpath, docpath);
    let modpath = litparse.apiPath(request.litpath, 'module');
    modpath = path.join(pubpath, modpath) + '.js';
    try
    {   var fields = await parseForm(request);
        let filedata = fields.filedata[0];
        if(!filedata)
        {   throw "no file";
        }
        // console.log(`putting ${docpath}`);
        let status = await litdoc.putDoc(filedata, docpath, modpath)
        return response.sendStatus(status);
    }
    catch(e)
    {   console.log('putdoc err: '+e);
        return response.sendStatus(400);
    }
}
async function putVersHandler(request, response)
{   let p = request.litpath;
    if(!p.username || !p.project || !p.version || p.file)
    {   return response.sendStatus(400);
    }
    let verspath = litparse.docPath(request.litpath, true);
    verspath = path.join(pubpath, verspath);
    let lprojpath = { 
        username: request.litpath.username,
        project: request.litpath.project
    };
    let projpath = litparse.docPath(lprojpath, true);
    projpath = path.join(pubpath, projpath);

    let status = await litdoc.publishVersion(projpath, verspath);

    return response.sendStatus(status);
}
async function metaHandler(request, response)
{   let filepath = litparse.docPath(request.litpath, true);
    filepath = path.join(pubpath, filepath);

    let meta = await litdoc.getMeta(request.litpath, filepath);
    response.writeHead(200, {'Conent-Type': 'application/json'}); 
    return response.end(JSON.stringify(meta));
}
async function moduleHandler(request, response)
{   let docpath = litparse.docPath(request.litpath);
    docpath = path.join(pubpath, docpath);
    let modpath = litparse.apiPath(request.litpath, 'module');
    modpath = path.join(pubpath, modpath) + '.js';

    console.log(request.litpath);
    console.log(docpath);
    console.log(modpath);
    let js = await litdoc.makeApi(request.litpath, docpath, modpath);
    console.log('js: '+(js?js.length:0));
    response.writeHead(js ? 200 : 404, {'Conent-Type': 'application/javascript'}); 
    if(js) response.end(js);
    else response.end();
}

// route middleware to make sure a user is logged in
function litPather(request, response, next) 
{   let p = request.params;
    let pv;
    let litpath = {};
    if(litparse.validUsername(p.username))
    {   litpath.username = p.username;
    }
    if(litparse.validProject(p.project))
    {   litpath.project = p.project;
    }
    if(litparse.validVersion(p.version))
    {   litpath.version = p.version;
    }
    if(pv = litparse.parseProjectVersion(p.projvers))
    {   litpath.project = pv.project;
        litpath.version = pv.version;
    }
    if(litparse.validFile(p.file))
    {   litpath.file = p.file;
    }
    request.litpath = litpath;
    return next();
}

function writeRestrict(request, response, next)
{   
    // console.log(`secure current user: ${request.user} path user: ${request.litpath.username}`);
    if(!request.user)
    {   return response.sendStatus(401);
    }
    // username string match access control - NSA hire me!
    if(request.litpath.username !== request.user.username)
    {   return response.sendStatus(403);
    }
    return next();
}

// app.get('/doc/u/:username', litPather, getDocHandler);
// app.get('/doc/u/:username/p/:project', litPather, getDocHandler);
// app.get('/doc/u/:username/p/:project/f/:file', litPather, getDocHandler);

app.post('/doc/u/:username/index', litPather, writeRestrict, putDocHandler);
app.post('/doc/u/:username/p/:project/index', litPather, writeRestrict, putDocHandler);
app.post('/doc/u/:username/p/:project/f/:file', litPather, writeRestrict, putDocHandler);

app.post('/doc/u/:username/p/:project/v/:version', litPather, ()=>response.sendStatus(403) );

app.post('/version/:username/:projvers', litPather, writeRestrict, putVersHandler);

app.get('/meta/:username', litPather, metaHandler);
app.get('/meta/:username/:projvers', litPather, metaHandler);
app.get('/meta/:username/:projvers/:file', litPather, metaHandler);

app.get('/module/:username\.js', litPather, moduleHandler);
app.get('/module/:username/:projvers\.js', litPather, moduleHandler);
app.get('/module/:username/:projvers/:file\.js', litPather, moduleHandler);


app.listen(8001);