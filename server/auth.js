
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
// const mysql = require('mysql')
const bcrypt = require('bcrypt')
const saltRounds = 8; // note: 2^n time cost, can't change after accounts exist

const db = require('litdb.js')
const util = require('util.js')
const nodeutil = require('util')

// const db_config = {
//     host     : 'localhost',
//     user     : 'root',
//     password : 'password',
//     multipleStatements: true
// };


const litparse = require('../public/litter/litparse.js')


const bcryptHash = nodeutil.promisify(bcrypt.hash);



// function initDatabase() {
//     var sql = `
// CREATE DATABASE litter;
// USE litter;
// CREATE TABLE user (
// id INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
// username VARCHAR(30) NOT NULL,
// passhash VARCHAR(127) NOT NULL,
// email VARCHAR(50),
// reg_date TIMESTAMP
// );`;
//     dbcon.query(sql, function (err, result) {
//         if (err) throw err;
//         console.log("Created User Table." + result);
//     });
// }


// var dbcon;

// function reconnectSql() {
//     dbcon = mysql.createConnection(db_config);

//     dbcon.connect(function(err) {   
//         if(err) {                   
//           console.log('error when connecting to db:', err);
//           setTimeout(reconnectSql, 2000);
//           return;
//         }
//         // verify database
//         var sql = `USE litter; SELECT 1 FROM user LIMIT 1;`;
//         dbcon.query(sql, function (err, result) {
//             if (err) {
//                 console.log(err);
//                 initDatabase();
//             } else {
//                 console.log("sql db connected!");
//             }
//         });      
//     });   

//     dbcon.on('error', function(err) {
//         if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
//             reconnectSql();                         
//         } else {         
//             console.log('db error', err);                           
//             throw err;                              
//         }
//     });
// }




exports.addToApp = function(app) {

    app.use(passport.initialize());
    app.use(passport.session());

    // reconnectSql();

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        // console.log('serialize '+JSON.stringify(user));
        done(null, user._id);
    });

    // used to deserialize the user
    passport.deserializeUser(async function(id, done) {
        id = String(id);
        let user = await db.getUserById(id);
        // console.log('deserialize '+id+' '+JSON.stringify(user));
        return done(null, user);
        // var findQuery = "SELECT * FROM user WHERE id = '"+id+"'";
        // dbcon.query(findQuery, function(err, rows) {   
        //     var user = rows.length ? rows[0] :  null;
        //     done(err, user);
        // });
    });

    function apiUser(dbuser) {
        return { id:dbuser.id, username:dbuser.username };
    }

    app.get("/null", function(request, response) {
        response.writeHead(200, {'Conent-Type': 'application/json'}); 
        response.end("null");
    })

    app.get("/echo", function(request, response) {
        let status = +request.query.status || 200;
        response.writeHead(status, {'Conent-Type': 'application/json'}); 
        response.end(JSON.stringify(request.query));
    })

    app.get("/whoami", function(request, response) {
        response.writeHead(200, {'Conent-Type': 'application/json'}); 
        if(request.user)
            response.end(JSON.stringify({user: request.user})); 
        else
            response.end("null");
    });

    // app.post("/register",  passport.authenticate('signup', { 
    //     successRedirect: '/whoami',
    //     failureRedirect: '/null',
    //     failureFlash: true
    // }));
    

    const passportOpt = {
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    };

    passport.use(new LocalStrategy(passportOpt, 
        async function(request, username, password, done) 
        {

            // User.findOne({ username: username }, function (err, user) {
            //   if (err) { return done(err); }
            //   if (!user) { return done(null, false); }
            //   if (!user.verifyPassword(password)) { return done(null, false); }
            //   return done(null, user);
            // });
            if(!litparse.validUsername(username))
            {   return done(null, null);
            }

            let user = await db.getUserByName(username);
            if(!user)
            {
                // response.writeHead(401, {'Conent-Type': 'application/json'}); 
                // let obj = {error: {message: 'No user found.'}};
                // return response.end(JSON.stringify(obj));
                console.log('no user '+username);
                return done(null, null);
            }

            let match = await bcrypt.compare(password, user.passhash);
            if(!match)
            {
                // response.writeHead(401, {'Conent-Type': 'application/json'}); 
                // let obj = {error: {message: 'Incorrect password.'}};
                // return response.end(JSON.stringify(obj));
                console.log('bad password');
                return done(null, null);
            }

            await new Promise((resolve)=>{
                request.login(user, resolve);
            });

            // response.writeHead(200, {'Conent-Type': 'application/json'}); 
            // response.end(JSON.stringify({ user:user }));
            return done(null, user);
        }
    ));

    app.post("/login",  passport.authenticate('local', 
    {   successRedirect: '/whoami',
        failureRedirect: '/echo?status=401&reason=bad+username+or+password'
    }));


    app.post('/register', async function(request, response)
    {   let username = request.body.username;
        let password = request.body.username;
        let invitecode = request.body.invitecode;

        if(!litparse.validUsername(username))
        {   return response.sendStatus(400);
        }

        let passhash = await bcrypt.hash(password, saltRounds);

        let registration = await db.registerUser(username, passhash, invitecode);

        console.log('registration '+JSON.stringify(registration));

        if(registration && registration.user)
        {   await new Promise((resolve)=>{
                request.login(registration.user, resolve);
            });
            response.writeHead(200, {'Conent-Type': 'application/json'}); 
            return response.end(JSON.stringify({user: registration.user}));
        }
        else if(registration.error.status)
        {   
            response.writeHead(registration.error.status, {'Conent-Type': 'application/json'}); 
            return response.end(JSON.stringify(registration));
        }
        response.writeHead(500, {'Conent-Type': 'application/json'}); 
        return response.end();
    });

    // app.post("/login", passport.authenticate('local-login', { 
    //     successRedirect: '/whoami',
    //     failureRedirect: '/null',
    //     failureFlash: true
    // }));
    app.get('/logout', function(request, response) 
    {   request.logout();
        response.sendStatus(200);
    });

    app.get('/invites', async function(request, response)
    {   if(!request.user)
        {   response.writeHead(401, {'Conent-Type': 'application/json'}); 
            return response.end();
        }
        if(request.user.canInvite && request.query.create == 1)
        {
            await db.createInvite(request.user, request.query.memo);
        }
        let invites = await db.getUserInvites(request.user);
        let showInvites = [];
        for(let inv of invites)
        {   showInvites.push({ code:inv.code, redeemUser:inv.redeemUser, memo:inv.memo })
        }
        response.writeHead(200, {'Conent-Type': 'application/json'}); 
        return response.end(JSON.stringify({invites: showInvites}));
    })

    // app.post('/login', async function(request, response)
    // {   let username = request.body.username;
    //     let password = request.body.username;

    //     if(!litparse.validUsername(username))
    //         done(null, null);

    //     let user = await db.getUserByName(username);
    //     if(!user)
    //     {
    //         response.writeHead(401, {'Conent-Type': 'application/json'}); 
    //         let obj = {error: {message: 'No user found.'}};
    //         return response.end(JSON.stringify(obj));
    //     }

    //     let match = await bcrypt.compare(password, user.passhash);
    //     if(!match)
    //     {
    //         response.writeHead(401, {'Conent-Type': 'application/json'}); 
    //         let obj = {error: {message: 'Incorrect password.'}};
    //         return response.end(JSON.stringify(obj));
    //     }

    //     await new Promise((resolve)=>{
    //         request.logIn(user, resolve);
    //     });

    //     response.writeHead(200, {'Conent-Type': 'application/json'}); 
    //     response.end(JSON.stringify({ user:user }));
    // })



    // app.post("/login", function(request, response, next) {
    //     passport.authenticate('login', function(err, user, info) {
    //         if (err) { return next(err); }
    //         if (!user) { 
    //             info = info || {error: "login failed"};
    //             response.writeHead(401, {'Conent-Type': 'application/json'}); 
    //             return response.end(JSON.stringify(info));
    //         }
    //         request.logIn(user, function(err) {
    //             if (err) { return next(err); }
    //             response.writeHead(200, {'Conent-Type': 'application/json'}); 
    //             return response.end(JSON.stringify({user: user})); 
    //         });
    //     })(request, response, next);
    // });

    // const passportOpt = {
    //     usernameField : 'username',
    //     passwordField : 'password',
    //     passReqToCallback : true
    // };

    // passport.use('login', new LocalStrategy(passportOpt,
    //     function(request, username, password, done) {
    //         if(!util.issafestr(username))
    //             done(null, null);

    //         dbcon.query("SELECT * FROM user WHERE username = '"+username+"'", function(err, rows) {
    //             if (err)
    //                 return done(err);

    //             if (!rows.length)
    //                 return done(null, null, {error: {message: 'No user found.'}}); 

    //             const dbuser = rows[0];
    //             bcrypt.compare(password, dbuser.passhash, function(err, match) {
    //                 if (err)
    //                     return done(err);

    //                 if (!match)
    //                     return done(null, null, {error: {message: 'Wrong password.'}});

    //                 return done(null, apiUser(dbuser));         
    //             });
    //         });
    //     }
    // ));

    // passport.use('signup', new LocalStrategy(passportOpt,
    //     async function(request, uuu, ppp, done) {
    //         // if(username != "k") // For now, block signups
    //         //     done(null, null);
    //         let username = request.body.username;
    //         let password = request.body.username;
    //         let invitecode = request.body.invitecode;

    //         if(!util.issafestr(username))
    //             done(null, null);

    //         // console.log(username);
    //         // console.log(password);
    //         // console.log(invitecode);

    //         let passhash = await bcrypt.hash(password, saltRounds);

    //         let registration = await db.registerUser(username, passhash, invitecode);

    //         console.log('registration '+JSON.stringify(registration));

    //         if(registration && registration.user)
    //         {   done(null, registration.user);
    //         }
    //         else
    //         {   done(null, null, registration);
    //         }

    //         // dbcon.query("SELECT * FROM user WHERE username = '"+username+"'", function(err, rows) {
    //         //     if (err)
    //         //         return done(err);

    //         //     if (rows.length)
    //         //         return done(null, null, {error: {message: 'Username taken.'}});

    //         //     var dbuser = { username : username };
    //         //     bcrypt.hash(password, saltRounds, function(err, hash) {
    //         //         if (err)
    //         //             return done(err);

    //         //         var insertQuery = "INSERT INTO user ( username, passhash ) values ('" + username +"','"+ hash +"')";
    //         //         dbcon.query(insertQuery, function(err, rows) {
    //         //             if (err)
    //         //                 return done(err);

    //         //             dbuser.id = rows.insertId;
    //         //             return done(null, apiUser(dbuser));
    //         //         });   
    //         //     });
    //         // });
    //     }
    // ));


}