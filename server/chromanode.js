var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');

function isstr(s) { return (typeof s)=='string'||(s instanceof String); }
function safestr(s) { return isstr(s) ? s.replace(/[^0-9A-Za-z\-]/g,"").substr(0,20) : ""; }
function fnv1a(s) { for(var a=0x811c9dc5,i=0;i<s.length;i++) { a^=s.charCodeAt(i); a=a*0x193+(a<<24); } return a>>>0; }
function contains(arr,o) { for(var i=0;i<arr.length;i++) { if(arr[i]==o) return true; } return false; }
function get(o,k) { for(var i=0,o2={};i<k.length;i++) { o2[k[i]]=o[k[i]]; } return o2; }
function errpre(pre) { return function(err){ if(err) console.log(pre+'::'+err); }; }
function tojson(o) { try { return JSON.stringify(o); } catch(err) { console.log('tojson::'+err+' '+o); return null; } }
function fromjson(o) { try { return JSON.parse(o); } catch(err) { console.log('fromjson::'+err+' '+o); return null; } }
function setroom(socket,room) { for(var r in socket.rooms) { socket.leave(r); } socket.join(room); }

function defaultRoom()
{
	return 'R0';
}


app.listen(1469);
function handler(request,response) { response.writeHead(200); response.end('chomanode alive'); }

io.on('connection', function(socket) {
	
	socket.emit('welcome', {
		defroom:defaultRoom(),
	})
	
	socket.on('update', function(data) {
		var id=safestr(data.id);
		if(!id) return;
		setroom(socket,id);
		
		//console.log(data);
		io.to(id).emit('update', {
			player:data.player, 
			note:data.note,
			rate:data.rate,
			enable:data.enable,
		})
		
	});
});
