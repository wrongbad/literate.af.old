
var currentUser = null;

function setUser(newuser)
{   currentUser = newuser;
    if(newuser)
    {
        dom.userbutton.innerHTML = newuser.username;
        dom.userbutton.href = '#'+newuser.username;
        loginError("");
        showLogin(false);
    }
    let nouser = !newuser;
    dom.userbutton.hidden = nouser;
    dom.logoutbutton.hidden = nouser;
    dom.loginbutton.hidden = !nouser;
    dom.regbutton.hidden = !nouser;
    dom.invitebutton.hidden = !(newuser && newuser.canInvite);
    updateNewLink();
}
function checkUser()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {   if (this.readyState == 4 && this.status == 200)
        {   var obj = JSON.parse(this.responseText);
            if(!obj || !obj.user)
            {   return;
            }
            setUser(obj.user);
        }
    };
    xhttp.open("GET", "/whoami", true);
    xhttp.send();
}
function loginError(text)
{
    dom.loginerror.innerHTML = text;
}
function showLogin(show, showInvite)
{   if(typeof show === 'undefined')
    {   show = dom.loginbar.hidden || (showInvite == dom.invitecode.hidden);
    }
    dom.loginbar.hidden = !show;
    dom.invitecode.hidden = !showInvite;
    dom.username.focus();
}
function showInvites(show)
{   if(typeof show === 'undefined')
    {   show = dom.invitebar.hidden;
    }
    dom.invitebar.hidden = !show;

    if(show)
    {   var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function()
        {   if (this.readyState == 4 && this.status == 200)
            {   var obj = JSON.parse(this.responseText);
                if(!obj || !obj.invites)
                {   return;
                }
                dom.invitelist.innerHTML = '';
                let hintText = (t) => {
                    let e = document.createElement('font');
                    e.color = '#aaa';
                    e.innerText = t;
                    return e;
                }
                let valText = (t) => document.createTextNode(t);
                for(let inv of obj.invites)
                {   let p = document.createElement('pre');
                    p.appendChild(hintText(' code: '));
                    p.appendChild(valText(inv.code));
                    p.appendChild(hintText(' redeemed: '));
                    p.appendChild(valText(inv.redeemUser));
                    p.appendChild(hintText(' memo: '));
                    p.appendChild(valText(inv.memo));
                    dom.invitelist.appendChild(p);
                }
            }
        };
        xhttp.open("GET", "/invites", true);
        xhttp.send();
    }
}
function newInvite()
{   let memo = dom.invitememo.value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {   if (this.readyState == 4 && this.status == 200)
        {   dom.invitememo.value = '';
            showInvites(true);
        }
    };
    xhttp.open("GET", "/invites?create=1&memo="+encodeURIComponent(memo), true);
    xhttp.send();
}

function logout()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {   if (this.readyState == 4 && this.status == 200) 
        {   setUser(null);
        }
    };
    xhttp.open("GET", "/logout", true);
    xhttp.send();
}
function login()
{    
    var un = dom.username.value;
    var pw = dom.password.value;
    if(!un || !pw)
        return;
    var invite;
    if(!dom.invitecode.hidden)
    {   invite = dom.invitecode.value;
    }

    var formData = { username: un, password: pw };
    if(invite)
    {   formData.invitecode = invite;
    }
    formData = encodeQueryData(formData);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {   if (this.readyState == 4)
        {   try
            {   var obj = JSON.parse(this.responseText); 
            } 
            catch(e) {}
            if(obj && obj.error)
            {   loginError(obj.error.message);
            } 
            else if(obj && obj.user)
            {   setUser(obj.user);
            }
            else
            {   loginError("login failed");
            }
        }
    };
    // force https
    // xhttp.open("POST", "https://whoiskylefinn.com/login/", true);
    xhttp.open("POST", invite?"/register/":'/login/', true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(formData);
}