
var acegutter = false;
function setAceGutter(gutter)
{   if(gutter == acegutter) return;

    var alleditors = getclass.celleditor;
    for(let i=0 ; i<alleditors.length ; i++)
    {   let editor = ace.edit(alleditors[i]);
        editor.renderer.setShowGutter(gutter);
    }
    acegutter = gutter;
}

var acefontsize = 11;
function setAceFontSize(size)
{   if(size == acefontsize) return;

    var alleditors = getclass.celleditor;
    for(let i=0 ; i<alleditors.length ; i++)
    {   let editor = ace.edit(alleditors[i]);
        editor.setOptions({ fontSize: size+"pt" });
    }
    acefontsize = size;
}


function cellText(cell)
{
    return cellEditor(cell).getValue();           
}

function setCellText(cell, celltext)
{
    cellEditor(cell).session.setValue(celltext);
}

function cellAfter(cell)
{
    return cell.nextSibling;
}

function cellBefore(cell)
{
    return cell.previousSibling;
}

// function cellType(cell)
// {
//     var celltype = cell.querySelector(".celltype");
//     return celltype.value;
// }

// function setCellType(cell, type)
// {
//     var celltype = cell.querySelector(".celltype");
//     celltype.value = type;
//     cellTypeChanged(cell, type);
// }

// const editModeMap = {
//     js: "javascript",
//     req: "javascript",
//     md: "markdown",
//     raw: "text",
// };

// function cellTypeChanged(cell, type)
// {
//     // var celledit = cell.querySelector(".celledit");
//     var cellscope = cell.querySelector(".cellscope");
//     var editor = cell.querySelector(".celleditor");
//     var cellresult = cell.querySelector(".cellresult");
//     // var celltypeL = cell.querySelector(".celltypeL");

//     // celltypeL.innerText = type;
//     cellscope.hidden = !["js","req"].includes(type);
//     // celledit.hidden = !["md"].includes(type)
    
//     if(!["md"].includes(type)) editor.hidden = false;
//     // celledit.innerText = editor.hidden ? "edit" : "done";

//     cellresult.innerHTML = "";


//     var editorSession = cellEditor(cell).getSession();
//     var editMode = editModeMap[type];
//     if(editMode)
//     {   editorSession.setMode("ace/mode/"+editMode);
//     }
//     // if(type!="js")
//     // {   editorSession.setUseWorker(false);
//     // }
// }

// function cellScope(cell)
// {
//     var cellscope = cell.querySelector(".cellscope");
//     return cellscope.value;
// }

// function setCellScope(cell, scope)
// {
//     var cellscope = cell.querySelector(".cellscope");
//     cellscope.value = scope;
//     cellScopeChanged(cell, scope);
// }
// function cellScopeChanged(cell, scope)
// {
//     // var cellscope = cell.querySelector(".cellscope");
//     // var cellscopeL = cell.querySelector(".cellscopeL");

//     if(scope == "api")
//         cell.classList.add('api');
//     else
//         cell.classList.remove('api');
//     // cellscopeL.innerText = scope;
// }

function cellResult(cell)
{
    var resultelem = cell.querySelector(".cellresult");
    return resultelem;
}

function cellEditor(cell)
{   if(!cell) return null;
    return ace.edit(cell.querySelector(".celleditor"));
}

function hoverCell(elem, hovering)
{
    cell = elem.closest(".cell");
    var cellhovers = cell.querySelectorAll(".cellhover");
    // var cellmodifier = cell.querySelector(".cellmodifier");
    for(let i=0 ; i<cellhovers.length ; i++)
    {   cellhovers[i].hidden = !hovering;
        // if(hovering) cellhovers[i].classList.add('open');
        // else cellhovers[i].classList.remove('open');
        // cellhovers[i].style.width = hovering ? "75px" : "0";
        // cellhovers[i].style.height = hovering ? "auto" : "0";
        // cellhovers[i].style.display = hovering ? "table-cell" : "block";
    }


    var cellhovers = cell.querySelectorAll(".hover");
    // var cellmodifier = cell.querySelector(".cellmodifier");
    for(let i=0 ; i<cellhovers.length ; i++)
    {   //cellhovers[i].hidden = !hovering;
        if(hovering) cellhovers[i].classList.add('open');
        else cellhovers[i].classList.remove('open');
    }

    // var editbut = cell.querySelector(".celledit3");

    // // editbut.innerText = hovering ? ">" : "<";
    // if(hovering) editbut.classList.add('open');
    // else editbut.classList.remove('open');
}

function addCell(button, below) 
{   var cell;
    if(button)
        cell = button.closest(".cell");
    // if(!cell)
    //     cell = dom.contentfooter;

    // var foot = document.querySelector("#contentfooter .cellheader");
    // foot.hidden = true;

    var newcell = getclass.cell[0].cloneNode(true);
    newcell.id = "";

    // TODO MVC
    newcell.model = {};

    // var celltype = newcell.querySelector(".celltype");
    // celltype.addEventListener('change', function(){
    //     cellTypeChanged(newcell, this.value);
    // });
    // var cellscope = newcell.querySelector(".cellscope");
    // cellscope.addEventListener('change', function(){
    //     cellScopeChanged(newcell, this.value);
    // });
    
    // let oldcelltype = cell.querySelector(".celltype");
    // let oldcellscope = cell.querySelector(".cellscope");
    // if(oldcelltype)
    //     celltype.value = oldcelltype.value;
    // if(oldcellscope)
    //     cellscope.value = oldcellscope.value;

    var editor = ace.edit(newcell.querySelector(".celleditor"));
    editor.setTheme("ace/theme/textmate");

    editor.setOptions({
        autoScrollEditorIntoView: true,
        maxLines: 200,
        printMargin: false,
        fontSize: acefontsize+"pt",
    });
    editor.commands.addCommand({
        name: 'execute',
        bindKey: 'Shift-Enter',
        exec: function(editor) { evalCell(newcell, function(){}); }
    });
    editor.renderer.setShowGutter(acegutter);
    // editor.on("focus", ()=>{expandCell(newcell, true);});
    editor.getSession().setUseWorker(false);
    editor.getSession().setUseWrapMode(true);
    editor.getSession().setMode("ace/mode/javascript");

    editor.setValue('');

    var lastCursor = 0;
    editor.selection.on("changeCursor", ()=>{
        let c = editor.selection.getCursor();
        setTimeout(()=>{ lastCursor=c }, 0);
    });

    editor.textInput.getElement().addEventListener("keydown", (e)=>{
        // let c = editor.selection.getCursor();
        let c = lastCursor;
        if(e.keyCode == 40) // down
        {   let n = editor.getSession().getDocument().getLength();
            if(c.row == n-1)
            {   let next = cellEditor(cellAfter(newcell));
                if(next) 
                {   next.gotoLine(1, c.column);
                    next.focus();
                    e.preventDefault();
                }
            }
        }
        else if(e.keyCode == 38) // up
        {   if(c.row == 0)
            {   let next = cellEditor(cellBefore(newcell));
                if(next) 
                {   let n = next.getSession().getDocument().getLength();
                    next.gotoLine(n, c.column);
                    next.focus();
                    e.preventDefault();
                }
            }
        }
        // console.log(e);
        // console.log(editor);
    });

    // cellTypeChanged(newcell, celltype.value);
    // cellScopeChanged(newcell, cellscope.value);

    if(!cell)
        dom.contentarea.appendChild(newcell);
    else if(below)
        dom.contentarea.insertBefore(newcell, cell.nextSibling);
    else
        dom.contentarea.insertBefore(newcell, cell);
    newcell.hidden = false;
    
    expandCell(newcell, true);

    return newcell;
}

function delCell(button) 
{   let cell = button.closest(".cell");
    cell.hidden = true;
    cell.model.deleted = true;
    updateCellCount();
}

function updateCellCount()
{   let cells = getclass.cell;
    let viscount = 0;
    for(let i=0 ; i<cells.length ; i++)
    {   viscount += cells[i].hidden ? 0 : 1;
    }
    // let foot = document.querySelector("#contentfooter .cellheader");
    // foot.hidden = (viscount > 0);
}

function updateNewLink()
{
    if(currentUser && litpath)
    {
        let username = currentUser.username;
        if(username == litpath.username && litpath.project)
        {   dom.newlink.href = '/#'+username+'/'+litpath.project+'/~';
        }
        else
        {   dom.newlink.href = '/#'+username+'/~';
        }
    }
    else
    {   dom.newlink.href = '/#~';
    }
}

function expandCell(button, expand)
{   var cell = button.closest(".cell");
    var editor = cell.querySelector(".celleditor");
    // var cellmodifier = cell.querySelector(".cellmodifier");
    // var celladd = cell.querySelector(".celladd");
    // var celltype = cell.querySelector(".celltype");
    // var cellscope = cell.querySelector(".cellscope");
    // var celltypeL = cell.querySelector(".celltypeL");
    // var cellscopeL = cell.querySelector(".cellscopeL");
    var celldel = cell.querySelector(".celldelete");
    var celledit = cell.querySelector(".celledit3");
    var cellresult = cell.querySelector(".cellresult");
    // maybe just append some damn properties on the cell???

    if(typeof expand === 'undefined')
        expand = cell.querySelector(".cellhover").hidden;

    // if(expand)
    hoverCell(cell, expand);

    // var hidescope = !["js","req"].includes(celltype.value);
    // var editorCanHide = ["md"].includes(celltype.value) || cell.hideEditor;
    var editorCanHide = cell.hideEditor;
    var editorWasHidden = editor.hidden;

    editor.hidden = !expand && editorCanHide;

    // if(editor.hidden && !editorWasHidden)
    //     evalCell(cell);

    // celledit.innerText = editor.hidden ? "edit" : "done";
    celledit.blur();

    // cellmodifier.hidden = !expand;
    // celladd.hidden = !expand;
    // celltype.hidden = !expand;
    // cellscope.hidden = hidescope ;//|| !expand;
    // celldel.hidden = !expand;

    // celltypeL.hidden = !celltype.hidden;
    // cellscopeL.hidden = hidescope ;//|| !cellscope.hidden;

    if(expand)
    {
        cells = getclass.cell; // ugly iteration for x-browser support
        for(let i=0 ; i<cells.length ; i++)
        {   let icell = cells[i];
            if(icell != cell)
            {   expandCell(icell, false);
            }
        }
        // ace.edit(editor).focus();
    }
}
