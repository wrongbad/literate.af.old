var intervalID = setInterval( ()=>{ 
    if(currentUser)
    {   saveProject(true); 
    }
}, 30000 );

var litpath = {};

function showProject(show, name)
{
    if(typeof show === 'undefined')
        show = dom.projectbar.hidden;
    dom.projectbar.hidden = !show;
    dom.projectfile.focus();
    // if(typeof name !== 'undefined')
    //     dom.projectlink.innerHTML = name;
}


function showVersion(show)
{
    if(typeof show === 'undefined')
        show = dom.versionbar.hidden;
    dom.versionbar.hidden = !show;
    dom.versiontext.focus();
    // if(typeof name !== 'undefined')
    //     dom.projectlink.innerHTML = name;
}

function saveVersion()
{
    let fname = dom.projectfile.value;
    let litpath = litparse.parsePath(fname);
    
    if(!litpath || !litpath.username || !litpath.project || litpath.file)
    {   return;
    }

    let version = dom.versiontext.value;
    if(!litparse.validVersion(version))
    {   dom.versionerror.innerText = 'version must follow #.#.# format'
        return;
    }

    litpath.version = version;

    let docpath = litparse.apiPath(litpath, 'version');
    // fname = fname + "@"+version;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {   if(this.readyState != 4) return;
        if(this.status == 200) 
        {   showVersion(false);
        }
        else if(this.status == 401)
        {   showLogin(true);
        }
    };
    xhttp.open("POST", docpath, true);
    xhttp.send();
}

var lastSavedJson = null;
var lastSavedObj = null;

function serializeToObj(fname)
{   var obj = {};
    obj.name = fname;
    obj.cells = [];
    // obj.deleteCells = [];
    cells = getclass.cell; // ugly iteration for x-browser support
    for(let i=0 ; i<cells.length ; i++)
    {   let cell = cells[i];
        if(cell != dom.templatecell)
        {   let cellobj = cell.model;
            // cellobj.type = cellType(cell);
            // cellobj.scope = cellScope(cell);
            if(cellobj.deleted)
            {   cellobj.text = '';
            }
            else
            {   cellobj.text = cellText(cell);
            }
            cellobj.hash = litparse.hashCode(cellobj.text);
            obj.hash = litparse.hashCode(cellobj.text, obj.hash);
            if(lastSavedObj) obj.prevHash = lastSavedObj.hash;
            obj.cells.push(cellobj);
        }
        // else if(cell && cell!=dom.templatecell)
        // {   obj.deleteCells.push(cell.cellobj)
        // }
    }
    return obj;
}

function saveProject(ifDifferent) 
{
    let fname = dom.projectfile.value;
    let litpath = litparse.parsePath(fname);
    if(!litpath)
        return;
    let docpath = litparse.docPath(litpath);

    var obj = serializeToObj(fname);
    var jsonString = JSON.stringify(obj);

    if(ifDifferent && lastSavedJson == jsonString)
    {   return;
    }   

    document.title = "saving..."

    var formData = new FormData();
    // formData.append("filename", fname);
    formData.append("filedata", jsonString);

    // var content = '<a id="a"><b id="b">hey!</b></a>'; 
    // var blob = new Blob([content], { type: "text/xml"});

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState != 4) return;
        if(this.status == 200) 
        {   history.replaceState(undefined, undefined, "#"+(fname));
            showProject(false, fname);
            for(let i=0 ; i<cells.length ; i++)
            {   let cell = cells[i];
                if(cell!=dom.templatecell)
                {   let cellobj = cell.model;
                    cellobj.prevHash = cellobj.hash;
                }
            }
            setTimeout(function() {
                document.title = fname;
            }, 500); 
            lastSavedJson = jsonString;
            lastSavedObj = obj;
            updatePathLinks(fname);
            updateNewLink();
        }
        else if(this.status == 401)
        {   showLogin(true);
            document.title = fname;
        }
        else if(this.status = 409)
        {   openProject(fname, true);
        }

    };
    xhttp.open("POST", docpath, true);
    xhttp.send(formData);
}

function updatePathLinks(fname)
{
    let litpath = litparse.parsePath(fname);
    if(!litpath)
    {   return;
    }
    let c = dom.toppath;
    c.innerHTML = '';

    let names = ['literate.af'];
    let links = ['/'];
    let linkCat = '#';
    if(litpath.username)
    {   linkCat += litpath.username;
        names.push(litpath.username)
        links.push(litpath.project ? linkCat : '');
    }
    if(litpath.project)
    {   let projvers = litpath.project
        if(litpath.version) projvers += '@'+litpath.version;
        linkCat += '/'+projvers;
        names.push(projvers)
        links.push(litpath.file ? linkCat : '');
    }
    if(litpath.file)
    {   names.push(litpath.file)
        links.push('');
    }

    for(let i=0 ; i<names.length ; i++)
    {   let a = document.createElement('a');
        if(links[i].length) a.href = links[i];
        else a.classList.add('fancylink');
        a.innerText = names[i];
        c.appendChild(a);
        c.innerHTML += ' ';
        if(i < 3)
        {
            a = document.createElement('a');
            a.innerText = '/ ';
            c.appendChild(a);
        }
    }
}

function mergeObjects(fname, serverObj)
{
    var clientObj = serializeToObj(fname);
    // remove server cells client knowingly modified
    for(cell of clientObj.cells)
    {   for(let i=0 ; i<serverObj.cells.length ; i++)
        {   if(cell.prevHash == serverObj.cells[i].hash)
            {   serverObj.cells.splice(i, 1);
                break;
            }
        }
    }
    // add unknown server cells back into client
    for(cell of serverObj.cells)
    {   if(!cell.deleted)
        {   clientObj.cells.push(cell);
            cell.prevHash = cell.hash;
        }
    }
    clientObj.prevHash = serverObj.hash;
    return clientObj;
}

async function openProject(fname, merge)
{
    let litpath = litparse.parsePath(fname);

    dom.verslink.hidden = !litpath || !litpath.project || litpath.file;
    if(dom.verslink.hidden)
    {   dom.versionbar.hidden = true;
    }
    updateNewLink();

    if(!litpath)
    {   return;
    }
    let docpath = litparse.docPath(litpath);
    let metapath = litparse.apiPath(litpath,'meta');

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = async function()
    {   if (this.readyState == 4)
        {   var obj;
            if(this.status == 200)
                obj = JSON.parse(this.responseText);
            else if(this.status == 404)
                obj = {name:fname, cells:[]};
            
            if(!obj || !obj.name || !obj.cells)
                return;
            litparse.modernize(obj);

            let loadObj = obj;
            if(merge)
            {   loadObj = mergeObjects(fname, obj);
            }
            
            lastSavedJson = this.responseText;
            lastSavedObj = obj;

            let list = getclass.cell;
            for(let i=list.length-1 ; i>=0 ; i--)
            {   if(list[i].id !== "templatecell")
                {   list[i].parentNode.removeChild(list[i]);
                }
            }
            resetInterpreter();
            for(let cellobj of loadObj.cells)
            {   if(cellobj.deleted) continue;
                let cell = addCell(null);
                // if(cellobj.type) setCellType(cell, cellobj.type);
                // if(cellobj.scope) setCellScope(cell, cellobj.scope);
                if(cellobj.text) setCellText(cell, cellobj.text);
                cell.model = cellobj;
                if(cell.model.hash)
                    cell.model.prevHash = cell.model.hash;

                expandCell(cell, false);
            }
            updateCellCount();
            await evalAll((err)=>{});
            if(fname != "lit")
            {   document.title = fname;
                history.replaceState(undefined, undefined, "#"+(fname));
            }
            showProject(false, fname);
        }
    };
    xhttp.open("GET", docpath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send();

    let xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = async function() 
    {   if (this.readyState == 4 && this.status == 200)
        {   let obj = JSON.parse(this.responseText);
            if(obj.error)
                console.log(obj.error);
            if(!obj.meta)
                return;
            obj = obj.meta;
            if(obj.contained)
            {   let c = dom.contained;
                c.innerHTML = '';
                for(let i=0;i<obj.contained.length;i++)
                {   let con=obj.contained[i];
                    let a = document.createElement('a');
                    a.href = '#'+fname+'/'+con;
                    a.innerText = con;
                    // a.target = '_blank';
                    c.appendChild(a);
                    c.innerHTML += ' '; // enables word wrap
                }
            }
            // if(obj.path)
            // {   let c = dom.toppath;
            //     c.innerHTML = '';
            //     let p = ['/'].concat(obj.path);
            //     let href = '';
            //     for(let i=0;i<p.length;i++)
            //     {   let a = document.createElement('a');
            //         if(p[i]=='/')
            //         {   a.href = '/';
            //             a.innerText = 'literate.af';
            //         }
            //         else
            //         {   href += (href ? '.' : '#') + p[i];
            //             if(href != '#'+fname)
            //             {   a.href = href;
            //             }
            //             else if(i>1)
            //             {   a.href = "javascript:showProject()"
            //             }
            //             a.innerText = p[i];
            //         }
            //         c.appendChild(a);
            //         c.innerHTML += ' '; // enables word wrap
            //     }
            //     let newlinkHref = '#'+obj.path[0];
            //     if(obj.path.length > 1) newlinkHref += '.'+obj.path[1];
            //     dom.newlink.href = newlinkHref+'.';
            // }
            if(obj.maxVersion)
            {   dom.versiontext.value = litparse.incrementPatch(obj.maxVersion);
            }
        }
    };
    xhttp2.open("GET", metapath, true);
    xhttp2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp2.send();
}




function resetInterpreter()
{
    liteval.cleanSlate();
    liteval.setvar("$exports", {});
    // 
    // liteval.setvar("document", document);
    // liteval.setvar("console", {
    //     log: function(x) { console.log(x); }
    // });
    // liteval.setvar("Math", Math);
    // liteval.setvar("Float32Array", Float32Array);
    // liteval.setvar("Float64Array", Float64Array);

    var stdlib = {
        Math: Math,
        Int32Array: Int32Array,
        Float32Array: Float32Array,
        Float64Array: Float64Array,
    }
    //global
    // _litvars_ = {
    //     $stdlib: stdlib,
    //     $exports: {},
    // };
    liteval.setvar("$stdlib", stdlib);
}

function noprint(val)
{
    const noprints = ["function","undefined"]
    return noprints.includes(typeof val);
}

var evalTypeMap = {};

evalTypeMap.raw = null;

evalTypeMap.md = async function(text, cellApi)
{   cellApi.resultElement.innerHTML = markdown.toHTML(text);
    // done();
}

evalTypeMap.req = async function(text, cellApi)
{
    var requireLibs = [];
    var requireVars = [];
    var lines = text.split("\n");
    for(let l of lines) if(l)
    {   var pair = /([\w-]+).*=.*"([\w-\/]+)"/.exec(l);
        if (pair && pair[1] && pair[2])
        {   requireVars.push(pair[1]);
            requireLibs.push(pair[2]);
        }
    }
    return new Promise((done) => {
        require(requireLibs, function(args)
        {   for(let i in requireVars)
            {   if(!arguments[i]) return done("load failed: "+requireLibs[i])
                liteval.setvar(requireVars[i], arguments[i]);

                var basenode = document.createElement("p");
                basenode.innerText = "loaded "
                basenode.style.margin = "0";
                var linknode = document.createElement("a");
                if(!requireLibs[i].includes("host/"))
                {
                    // linknode.href = "#"+encodeURIComponent(requireLibs[i])
                    linknode.href = "#"+requireLibs[i];
                }
                linknode.target = "_blank";
                linknode.innerText = requireLibs[i];
                basenode.appendChild(linknode);
                cellApi.appendChild(basenode);
            }
            done();
        });
    });
}

async function asyncRequire(requireLibs)
{   return new Promise((resolve) => {
        require(requireLibs, function(args)
        {   resolve(arguments);
        });
    });
}

function markdownElem(mdString)
{
    let d = document.createElement('div');
    d.innerHTML = markdown.toHTML(mdString);
    return d;
}

evalTypeMap.js = async function(text, cellApi)
{
    let magicVars = {
        $cell: cellApi,
        md: markdownElem,
    };
    try
    {   let res = liteval.eval(text, magicVars);
        if(res instanceof Element)
        {   cellApi.appendChild(res);
        }
        else if(!noprint(res) && (res = String(res)))
        {   cellApi.log(res);
        }
    } 
    catch(e)
    {   console.log(e);
    }
}

async function evalDirectives(cell, text, cellApi)
{
    var dirs = litparse.getDirectives(text);
    let hide = false;
    let api = false;
    let reqvars = [];
    let reqlibs = [];

    let handlers = {
        hide: ()=>{ hide=true; },
        importAs: (lib, name)=>{ reqlibs.push(lib); reqvars.push(name); },
        api: ()=>{ api=true; },
    };

    litparse.execDirectives(dirs, handlers);

    if(hide)
    {   cell.hideEditor = true;
        if(cell.querySelector(".cellhover").hidden)
        {   expandCell(cell, false);
        }
    }

    if(api) cell.classList.add('api');
    else cell.classList.remove('api');

    if(reqlibs.length)
    {
        reqobjs = await asyncRequire(reqlibs);
        for(let i in reqvars)
        {   if(!reqobjs[i]) throw "load failed: "+requireLibs[i];
            liteval.setvar(reqvars[i], reqobjs[i]);

            var basenode = document.createElement("p");
            basenode.innerText = "loaded "
            basenode.style.marginTop = "0";
            var linknode = document.createElement("a");
            if(!reqlibs[i].startsWith("host/"))
                linknode.href = "#"+reqlibs[i];
            linknode.target = "_blank";
            linknode.innerText = reqlibs[i];
            basenode.appendChild(linknode);
            cellApi.appendChild(basenode);
        }
    }
}

async function evalAll()
{   var cells = [];
    var list = getclass.cell;
    for(let i=0 ; i<list.length ; i++)
    {   if(list[i].id !== "templatecell")
        {   await evalCell(list[i], ()=>{});
        }
    }
}

async function evalCell(cell)
{   cell = cell.closest(".cell");
    // var celltype = cellType(cell);
    var resultelem = cellResult(cell);
    var celltext = cellEditor(cell).getValue();
    // var evalfunc = evalTypeMap[celltype];
    var evalfunc = evalTypeMap.js;
    var cellApi = new CellApi(resultelem);
    cellApi.clear();
    
    await evalDirectives(cell, celltext, cellApi);

    if(evalfunc)
    {   await evalfunc(celltext, cellApi);
    }
}
