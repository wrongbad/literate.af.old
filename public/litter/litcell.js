

class CellApi
{   constructor(resultElement)
    {   this.resultElement = resultElement;
    }
    createElement(type)
    {   return this.resultElement.ownerDocument.createElement(type);
    }
    appendChild(element)
    {   
        // if(this.resultElement.innerHTML.length == 0)
        // {   let p = this.createElement('div');
        //     p.style.height = "15px";
        //     this.resultElement.appendChild(p);
        // }
        this.resultElement.appendChild(element);
    }
    clear()
    {
        this.resultElement.innerHTML = "";
    }
    log(stuff)
    {   var p = this.createElement("pre");
        p.innerText = String(stuff);
        p.style.margin = "0px";
        this.appendChild(p);
    }
}