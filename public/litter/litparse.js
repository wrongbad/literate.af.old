
var litparse = litparse || {};

if((typeof exports)==='undefined')
{   exports = litparse;
}

(function() {

    exports.getDirectives = function(text)
    {   let rgx = /`([^\\`]*\\[^])*[^\\`]*`/g;
        // empty out all template literal quotes
        // so we don't match the stuff below inside ``
        text = text.replace(rgx,'');

        // match '//!abc 1 2 3'
        let dirs = [];
        let match = null;
        rgx = /^\s*\/\/!([a-z]+.*)$/gm;
        while ((match = rgx.exec(text)))
        {   let d = match[1].trim();
            if(!dirs.includes(d))
            {   dirs.push(d);
            }
        }
        return dirs;
    }


    /**
     * handlers = {
     *  importAs : function(lib, name)
     *  hide : function()
     *  api : function()
     * }
     * all are optional
     */
    exports.execDirectives = function(dirs, handlers)
    {   for(let d of dirs)
        {   let dword = d.split(/\s+/);
            if(!dword.length) continue;
            if(dword[0]=='hide' && handlers.hide)
            {   handlers.hide();
            }
            else if(dword[0]=='import' && handlers.importAs)
            {   if(dword.length==4 && dword[2]=='as')
                {   handlers.importAs(dword[1], dword[3]);
                }
                else if(dword.length==2)
                {   handlers.importAs(dword[1], dword[1]);
                }
                else
                {   console.log("only 'import lib as name' syntax supported");
                }
            }
            else if(dword[0]=='api' && handlers.api)
            {
                handlers.api();
            }
        }
    }

    var userRegex = '[a-zA-Z0-9\-_]{1,20}';
    var projectRegex = '[a-zA-Z][a-zA-Z0-9\-_]{0,20}';
    var versionRegex = '(0|[1-9][0-9]{0,2})\.(0|[1-9][0-9]{0,2})\.(0|[1-9][0-9]{0,2})';
    var fileRegex = projectRegex;
    // var sepRegex = '\\.';
    var sepRegex = '/';

    exports.parseProjectVersion = function(str)
    {   if(!str) return null;
        let match = str.match(new RegExp(`^(${projectRegex})(@(${versionRegex}))?$`));
        if(match)
        {   return {   
                project: match[1],
                version: match[3],
                vers1: match[4],
                vers2: match[5],
                vers3: match[6],
                versionInt: (parseInt(match[4]) * 1000000 + parseInt(match[5]) * 1000 + parseInt(match[6]))|0,
            };
        }
        return null;
    }

    exports.parseVersion = function(str)
    {   let match = str.match(new RegExp(`^(${versionRegex})$`));
        if(match)
        {   return {   
                version: match[1],
                vers1: match[2],
                vers2: match[3],
                vers3: match[4],
                versionInt: (parseInt(match[2]) * 1000000 + parseInt(match[3]) * 1000 + parseInt(match[4]))|0,
            };
        }
        return null;
    }

    exports.hashCode = function(str, accumHash)
    {   return str.split('').reduce((prevHash, currVal) =>
            (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, accumHash||0);
    }

    exports.incrementPatch = function(str)
    {   let match = str.match(new RegExp(`^${versionRegex}$`));
        if(match)
        {   let patch = parseInt(match[3]);
            if(patch < 999)
            {   return `${match[1]}.${match[2]}.${patch+1}`;
            }
        }
        return null;
    }

    exports.validUsername = function(str)
    {   return str && !!(str.match(new RegExp(`^${userRegex}$`)));
    }
    exports.validProject = function(str)
    {   return str && !!(str.match(new RegExp(`^${projectRegex}$`)));
    }
    exports.validVersion = function(str)
    {   return str && !!(str.match(new RegExp(`^${versionRegex}$`)));
    }
    exports.validFile = function(str)
    {   return str && !!(str.match(new RegExp(`^${fileRegex}$`)));
    }
    exports.parsePath = function(str)
    {   //                       1                           2                    3    (4,5,6)                     7 
        let regex = new RegExp(`^(${userRegex})(?:${sepRegex}(${projectRegex})(?:@(${versionRegex}))?(?:${sepRegex}(${fileRegex}))?)?$`);
        let match = str.match(regex);
        if(match)
        {   return {
                username: match[1],
                project: match[2],
                version: match[3],
                vers1: match[4],
                vers2: match[5],
                vers3: match[6],
                file: match[7],
            };
        }
        return null;
    }
    exports.docPath = function(litpath, noIndex)
    {   let path = '/doc/u/'+litpath.username;
        if(litpath.project) path += '/p/'+litpath.project;
        if(litpath.version) path += '/v/'+litpath.version;
        if(litpath.file) path += '/f/'+litpath.file;
        else if(!noIndex) path += '/index';
        return path;
    }
    exports.apiPath = function(litpath, prefix)
    {   let path = '/'+prefix+'/'+litpath.username;
        if(litpath.project) path += '/'+litpath.project;
        if(litpath.version) path += '@'+litpath.version;
        if(litpath.file) path += '/'+litpath.file;
        return path;
    }
    exports.moduleName = function(litpath)
    {   let path = litpath.username;
        if(litpath.project) path += '/'+litpath.project;
        if(litpath.version) path += '@'+litpath.version;
        if(litpath.file) path += '/'+litpath.file;
        return path;
    }

    exports.modernize = function(project)
    {   for(let cell of project.cells)
        {   if(cell.type == 'req')
            {   let lines = cell.text.split("\n");
                let newtext = '';
                for(let l of lines) if(l)
                {   // includes quotes in libname
                    let pair = /([\w-]+).*=.*"([\w-\/]+)"/.exec(l);
                    if (pair && pair[1] && pair[2])
                    {   if(newtext.length) newtext+='\n';
                        newtext += `//!import ${pair[2]} as ${pair[1]}`;
                    }
                }
                cell.text = newtext;
            }
            if(cell.scope == 'api')
            {   cell.text = '//!api\n' + cell.text;
            }
            delete cell.scope;
            delete cell.type;
        }
    }

})();