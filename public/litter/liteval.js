var liteval = liteval || (function () {

// private:

    var exports = {};

    // Proxy magic
    // Outer vars layer captures all variable read/writes
    // "with (vars) { code }"

    //global
    _litvars_ = {};

    // var vars_ = {};
    // var vars = new Proxy(vars_, { 
    //     get: function(obj, prop, receiver) {
    //         if(prop === Symbol.unscopables)
    //             return []; // "with" queries this, we say all is scopable
    //         else
    //             return obj[prop];
    //     },
    //     set: function(obj, prop, val, receiver) {
    //         obj[prop] = val;
    //     },
    //     has: function(obj, prop) {
    //         return '_litcell_'!==prop;
    //         // return true;
    //     },
    // });

// public:

    exports.cleanSlate = function()
    {
        _litvars_ = {};
        // for(i in vars_)
        // {
        //     delete vars_[i];
        // }
    }

    exports.eval = function(expr, cellContext)
    {
        // global
        _litcell_ = cellContext;
        try {
            // expr = 'with (vars) with (static_) {'+expr+'}'
            // expr = 'with (vars) {'+expr+'}'
            // var expr = "with (_litvars_) with (_litcell_) {\n"+expr+"\n}"; 

            // note the /**/ at then end enables a cell to be commented
            // by a single /* at the beginning
            var expr = 'with (_litvars_) with (_litcell_) {\n'+expr+'\n/**/}';
            var res = eval.call(null, expr); // calls in global scope
            // var res = eval(expr);
            delete _litcell_;
            return res;
            // return eval(expr);
        } catch(e) {
            console.log(e);
            console.log("Caused By: "+expr);
            delete _litcell_;
            return e;
        }
    }

    exports.setvar = function(name, value)
    {
        _litvars_[name] = value;
    }

    exports.getvar = function(name)
    {
        return _litvars_[name];
    }

    exports.allvars = function()
    {
        return _litvars_;
    }

    return exports;

})();