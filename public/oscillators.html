<!DOCTYPE html>
<html>
<head>
<title>Oscillators</title>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.6.0/katex.min.css">
</head>
<body style="width:100%">
	<div id="cc" style="width:1000px;">
		<div id="toplinks"><a href="./">home</a></div>
		<div style="display:inline-block;width:900px;text-align:left;">
			<h3>Efficient Closed-Form Band-Limited Triangle, Square, and Sawtooth Waves</h3>
			<h4>Kyle Finn</h4>
			<p>The method is based on previous work identifying simple closed form equations for a finite sum of harmonics with constant or exponential decaying coefficients (e.g. a band limited impulse train "BLIT"), and using that to closely approximate inverse linear and inverse quadratically decaying coefficients (e.g. triangle, square, and saw waves).
			</p>
			<p>The motivation for this lies in the weaknesses of alternative methods for generating such waves. Methods relying on integration struggle to stay centered under frequency modulation as there is interesting transient behavior. Wavetables can require a lot of memory and have a trade-off between interpolation kernel performance and alias-limiting. Up-sampling and filtering methods have a similar tradeoff and both can sound muffled as they tend to roll-off early at the high end of the spectrum. And simple additive synthesis even with trig identity optimizations becomes inefficient compared to this method above 16 harmonics, which is inadequate fidelity for most applications. This method aims to address all these drawbacks by providing a closed form computation per sample which has constant-time performance, no transient instability, and absolutely zero aliasing for constant frequency input.
			</p>
			<p>The previously known closed form equation for BLIT functions, with f (frequency in Hertz), t (time in seconds), and n (number of harmonics, generally (sample rate)/(2f)) is:
			</p>
			$$ \text{Let} \quad \theta = (2\pi f \cdot t + \phi) $$
			$$ \text{blit}_{n}(\theta) = \sum_{k=0}^{n} \frac{1}{n} \cos(k\theta) = \frac{\sin((2n+1)\frac{\theta}{2})}{2n \cdot \sin(\frac{\theta}{2})} $$
			
			<p>Here is a graph of that function at n=15</p>
			
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1.5" data-ymin="-.5" data-function="{n=15;sin((2*n+1)*x/2)/(2*n*sin(x/2));}"></div>
			
			<p>Now that we have established this function, let's look at the Fourier series for a band limited sawtooth wave:</p>
			
			$$ \text{saw}_n(\theta) = \frac{1}{\pi} \sum_{k=1}^{n} \frac{1}{k} \sin(k\theta) $$
			
			<p>Here is a plot of 1/k (in red), five A exp(-Bk) functions (in blue), and their sum (in green), up to k=100 on a logarithmic y axis:</p>

			<div class="graph" style="text-align: center;" data-xmin="0" data-xmax="100" data-ymin="-6" 
				data-color="#ff0000" data-function="y=ln(1/x)"
				data-color1="#0000ff" 
				data-function1="y=ln(2.3007863063649596*exp(-1.8297771828979625*x))"
				data-function2="y=ln(0.6565885048915368*exp(-0.5556164391545395*x))"
				data-function3="y=ln(0.2082326302527493*exp(-0.1644857716734673*x))"
				data-function4="y=ln(0.06326370974252625*exp(-0.042326104177807695*x))"
				data-function5="y=ln(0.01695235197484182*exp(-0.006266059859282106*x))"
				data-color6="#00ff00"
				data-function6="y=ln(
				+2.3007863063649596*exp(-1.8297771828979625*x)
				+0.6565885048915368*exp(-0.5556164391545395*x)
				+0.2082326302527493*exp(-0.1644857716734673*x)
				+0.06326370974252625*exp(-0.042326104177807695*x)
				+0.01695235197484182*exp(-0.006266059859282106*x))">
			</div>
			<p>That tells us we can approximate the sawtooth function very accurately like so:</p>
			
			$$ \text{saw}_n(\theta) = \frac{1}{\pi} \sum_{k=1}^{n} \frac{1}{k} \sin(k\theta) \approx A_1 \sum_{k=1}^{n} e^{-B_1 k} \sin(k\theta) + A_2 \sum_{k=1}^{n} e^{-B_2 k} \sin(k\theta) + ...$$
			
			<p>Finding the closed form calculation of that new sum...</p>
 
			$$ A \sum_{k=1}^{n} e^{-Bk} \sin(k\theta) $$ 
			
			$$ = A \cdot \text{Imag} \left\{ \sum_{k=1}^{n} e^{-Bk} e^{ki\theta} \right\} $$
			
			$$ = A \cdot \text{Imag} \left\{ \sum_{k=1}^{n} e^{(-B+i\theta)^{k}} \right\} $$ 
			
			$$ = A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - e^{(n+1)(-B+i\theta)} }
			{ 1 - e^{-B+i\theta} } \right \} $$
			
			$$ = A \cdot \text{Imag} \left \{ \frac{ (e^{-B+i\theta} - e^{(n+1)(-B+i\theta)})(1 - e^{(-B-i\theta)})  }
			{ (1 - e^{-B+i\theta})(1 - e^{-B-i\theta}) } \right \} $$
			
			$$ = A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - e^{(n+1)(-B+i\theta)} - e^{-B+i\theta} e^{-B-i\theta}) + e^{(n+1)(-B+i\theta)} e^{(-B-i\theta)})  }
			{ 1 - e^{-B+i\theta} - e^{-B-i\theta} + e^{-2B} } \right \} $$
			
			$$ = A \cdot \text{Imag} \left \{ \frac{ e^{-B} e^{i\theta} - e^{-(n+1)B} e^{(n+1)i\theta} - e^{-2B} + e^{-(n+2)B} e^{((n)i\theta)}  }
			{ 1 - e^{-B}(e^{i\theta} - e^{-i\theta}) + e^{-2B} } \right \} $$
			
			$$ = A \cdot \frac{e^{-B} \sin(\theta) - e^{-(n+1)B} \sin(n\theta+\theta) + e^{-(n+2)B} \sin(n\theta) }
				{1-2e^{-B} cos(\theta)+e^{-2B}} $$
				

			<p>Putting that back into our sawtooth approximation, we get this closed form sawtooth equation</p>
			
			$$ \text{saw}_n(\theta) = \frac{1}{\pi} \sum_{k=1}^{5} A_k \cdot \frac{e^{-B_k} \sin(\theta) - e^{-(n+1)B_k} \sin(n\theta+\theta) + e^{-(n+2)B_k} \sin(n\theta) }			{1-2e^{-B_k} cos(\theta)+e^{-2B_k}}$$
			
			<p>Computationally, it requires 4 trig functions per sample, and the exponentials only need to be recomputed when the number of harmonics changes. Additionally, the trig functions can be computed algebraically from the previous sample using the sin-cos angle-add equations.</p>

			<p>Here is a plot of that sawtooth wave approximation using the same A and B coefficients from above, with n=35</p>
				
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{
				let A=[2.3007863063649596,0.6565885048915368,0.2082326302527493,0.06326370974252625,0.01695235197484182];
				let B=[1.8297771828979625,0.5556164391545395,0.1644857716734673,0.042326104177807695,0.006266059859282106];
				let n=35;
				(A[0]*(( exp(-B[0])*sin(x) - exp(-(n+1)*B[0])*sin((n+1)*x) + exp(-(n+2)*B[0])*sin((n)*x) ) / ( 1 - 2*exp(-B[0])*cos(x) + exp(-2*B[0]) ))
				+A[1]*(( exp(-B[1])*sin(x) - exp(-(n+1)*B[1])*sin((n+1)*x) + exp(-(n+2)*B[1])*sin((n)*x) ) / ( 1 - 2*exp(-B[1])*cos(x) + exp(-2*B[1]) ))
				+A[2]*(( exp(-B[2])*sin(x) - exp(-(n+1)*B[2])*sin((n+1)*x) + exp(-(n+2)*B[2])*sin((n)*x) ) / ( 1 - 2*exp(-B[2])*cos(x) + exp(-2*B[2]) ))
				+A[3]*(( exp(-B[3])*sin(x) - exp(-(n+1)*B[3])*sin((n+1)*x) + exp(-(n+2)*B[3])*sin((n)*x) ) / ( 1 - 2*exp(-B[3])*cos(x) + exp(-2*B[3]) ))
				+A[4]*(( exp(-B[4])*sin(x) - exp(-(n+1)*B[4])*sin((n+1)*x) + exp(-(n+2)*B[4])*sin((n)*x) ) / ( 1 - 2*exp(-B[4])*cos(x) + exp(-2*B[4]) )))/pi
				;}"></div>
			
			<p>For comparison, here is the exact version at n=35</p>
				
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{y=0;for(var jj=1;jj!=36;jj++){y+=sin(jj*x)/(jj*pi)};y}"></div>
			
			<p>Moving onward, let's look at the Fourier series for a band limited square wave, noting that it has the same harmonic rolloff so we can use the same coefficients as the sawtooth wave, but we need a new forumla for an odd-only cosine sum</p>
			
			$$ \text{square}_n(\theta) = \frac{2}{\pi} \sum_{k=1}^{\lceil n/2 \rceil} \frac{1}{2k-1} \sin((2k-1)\theta) $$
			
			<p>The function we're looking for</p>
			
			$$ A \sum_{k=1}^{\lceil n/2 \rceil} e^{-B(2k-1)} \sin((2k-1)\theta) $$
			
			$$ A \cdot \text{Imag} \left\{ \sum_{k=1}^{\lceil n/2 \rceil} e^{-B(2k-1)} e^{(2k-1)i\theta} \right\} $$
			
			$$ A \cdot \text{Imag} \left\{ \sum_{k=1}^{\lceil n/2 \rceil} e^{(-B+i\theta)^{(2k-1)}} \right\} $$
			
			$$ \text{Let} \quad m=\{n \quad \text{if n is odd or} \quad n-1 \quad \text{if n is even}\} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - e^{(m+2)(-B+i\theta)}}
			{ 1 - e^{2(-B+i\theta)} } \right \} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ (e^{-B+i\theta} - e^{(m+2)(-B+i\theta)}) \cdot (1 - e^{2(-B-i\theta)}) }
			{ (1 - e^{2(-B+i\theta)})(1 - e^{2(-B-i\theta)}) } \right \} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - e^{(m+2)(-B+i\theta)} - e^{-B+i\theta} e^{2(-B-i\theta)} + e^{(m+2)(-B+i\theta)} e^{2(-B-i\theta)} }
			{ 1 - e^{2(-B+i\theta)} - e^{2(-B-i\theta)} + e^{-4B} } \right \} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B} e^{i\theta} - e^{-(m+2)B} e^{(m+2) i\theta} - e^{-3B} e^{-i\theta} + e^{-(m+4)B} e^{(m)i\theta} }
			{ 1 - e^{-2B}(e^{2i\theta} + e^{-2i\theta}) + e^{-4B} } \right \} $$

			$$ A \cdot \frac{ e^{-B} \sin(\theta) - e^{-(m+2)B} \sin((m+2)\theta) + e^{-3B} \sin(\theta) + e^{-(m+4)B} \sin((m)\theta) }
			{ 1 - 2e^{-2B}cos(2\theta) + e^{-4B} } $$
			
			<p>Putting that back into our square approximation, we get (where m is odd)</p>
	
			$$ \text{square}_m(\theta) = \frac{2}{\pi} \sum_{k=1}^{5} A_k \cdot \frac{ (e^{-3B_k}+ e^{-B_k}) \sin(\theta) - e^{-(m+2)B_k} \sin((m+2)\theta) + e^{-(m+4)B_k} \sin(m\theta)}
			{1-2e^{-2B_k} cos(2\theta) + e^{-4B_k}} $$
	
			<p>And that looks like this at n=25</p>
			
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{
				let A=[2.3007863063649596,0.6565885048915368,0.2082326302527493,0.06326370974252625,0.01695235197484182];
				let B=[1.8297771828979625,0.5556164391545395,0.1644857716734673,0.042326104177807695,0.006266059859282106];
				let m=25;
				(A[0]*(( (exp(-B[0])+exp(-3*B[0]))*sin(x) + exp(-(m+4)*B[0])*sin((m)*x) - exp(-(m+2)*B[0])*sin((m+2)*x) ) / ( 1 - 2*exp(-2*B[0])*cos(2*x) + exp(-4*B[0]) ))
				+A[1]*(( (exp(-B[1])+exp(-3*B[1]))*sin(x) + exp(-(m+4)*B[1])*sin((m)*x) - exp(-(m+2)*B[1])*sin((m+2)*x) ) / ( 1 - 2*exp(-2*B[1])*cos(2*x) + exp(-4*B[1]) ))
				+A[2]*(( (exp(-B[2])+exp(-3*B[2]))*sin(x) + exp(-(m+4)*B[2])*sin((m)*x) - exp(-(m+2)*B[2])*sin((m+2)*x) ) / ( 1 - 2*exp(-2*B[2])*cos(2*x) + exp(-4*B[2]) ))
				+A[3]*(( (exp(-B[3])+exp(-3*B[3]))*sin(x) + exp(-(m+4)*B[3])*sin((m)*x) - exp(-(m+2)*B[3])*sin((m+2)*x) ) / ( 1 - 2*exp(-2*B[3])*cos(2*x) + exp(-4*B[3]) ))
				+A[4]*(( (exp(-B[4])+exp(-3*B[4]))*sin(x) + exp(-(m+4)*B[4])*sin((m)*x) - exp(-(m+2)*B[4])*sin((m+2)*x) ) / ( 1 - 2*exp(-2*B[4])*cos(2*x) + exp(-4*B[4]) )))*2/pi
				;}"></div>
				
			<p>For comparison, here is the exact square wave at n=25</p>
			
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{y=0;for(var jj=1;jj!=27;jj+=2){y+=2*sin(jj*x)/(jj*pi)};y}"></div>
			
			<p>And finally, moving on the the triangle wave:</p>
			
			$$ \text{triangle}_n(\theta) = \frac{4}{\pi^2} \sum_{k=1}^{\lceil n/2 \rceil} \frac{(-1)^{k-1}}{(2k-1)^2} \sin((2k-1)\theta) $$
			
			<p>Since we're now dealing with k^-2 frequency rolloff instead of k^-1, we need a new set of exponential coefficients</p>
			
			<p>Here is a plot of k^-2 (in red), five A exp(-Bk) functions (in blue), and their sum (in green), up to k=100 on a logarithmic y axis:</p>

			<div class="graph" style="text-align: center;" data-xmin="0" data-xmax="100" data-ymin="-12" 
				data-color="#ff0000" data-function="y=ln(1/x/x)"
				data-color1="#0000ff" 
				data-function1="y=ln(7.696628911325569*exp(-2.672325718313832*x))"
				data-function2="y=ln(0.9594497374687989*exp(-0.9970754544305241*x))"
				data-function3="y=ln(0.14181977912419547*exp(-0.3629940341223481*x))"
				data-function4="y=ln(0.016299642072186815*exp(-0.1108529795869248*x))"
				data-function5="y=ln(0.0010188018664029279*exp(-0.0225846793201704*x))"
				data-color6="#00ff00"
				data-function6="y=ln(7.696628911325569*exp(-2.672325718313832*x)
				+0.9594497374687989*exp(-0.9970754544305241*x)
				+0.14181977912419547*exp(-0.3629940341223481*x)
				+0.016299642072186815*exp(-0.1108529795869248*x)
				+0.0010188018664029279*exp(-0.0225846793201704*x))">
			</div>
			
			<p>Same method again, using this function</p>
			
			$$ A \sum_{k=1}^{\lceil n/2 \rceil} (-1)^{k-1} e^{-B(2k-1)} \sin((2k-1)\theta) $$
			
			$$ A \cdot \text{Imag} \left\{ \sum_{k=1}^{\lceil n/2 \rceil} (-1)^{k-1} e^{-B(2k-1)} e^{(2k-1)i\theta} \right\} $$
			
			$$ A \cdot \text{Imag} \left\{ \sum_{k=1}^{\lceil n/2 \rceil} (-1)^{k-1} e^{(-B+i\theta)^{(2k-1)}} \right\} $$
			
			$$ \text{Let} \quad m=\{n \quad \text{if n is odd or} \quad n-1 \quad \text{if n is even}\} $$
			$$ \text{Let} \quad t = (-1)^{(m-1)/2} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - t \cdot e^{(m+2)(-B+i\theta)}}
			{1 + e^{2(-B+i\theta)}} \right \} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ ( e^{-B+i\theta} - t \cdot e^{(m+2)(-B+i\theta)}) \cdot (1+e^{2(-B-i\theta)}) }
			{(1 + e^{2(-B+i\theta)})(1+e^{2(-B-i\theta)})} \right \} $$
				
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B+i\theta} - t \cdot e^{(m+2)(-B+i\theta)} + e^{2(-B-i\theta)} e^{(-B+i\theta)} - t \cdot e^{2(-B-i\theta)} e^{(m+2)(-B+i\theta)} }
			{ 1 + e^{-2B} (e^{2i\theta} + e^{-2i\theta}) + e^{-4B} } \right \} $$
			
			$$ A \cdot \text{Imag} \left \{ \frac{ e^{-B} e^{i\theta} - t \cdot e^{-(m+2)B} e^{(m+2)i\theta} + e^{-3B} e^{-i\theta} - t \cdot e^{-(m+4)B} e^{(m)i\theta} }
			{ 1 + e^{-2B} (e^{2i\theta} + e^{-2i\theta}) + e^{-4B} } \right \} $$
			
			$$ A \cdot \frac{ e^{-B} \sin(\theta) - t \cdot e^{-(m+2)B} \sin((m+2)\theta) - e^{-3B} \sin(\theta) - t \cdot e^{-(m+4)B} \sin(m\theta) }
			{ 1 + 2e^{-2B} cos(2\theta) + e^{-4B} } $$
			
			$$ A \cdot \frac{ (e^{-B}- e^{-3B}) \sin(\theta) - (-1)^{(m-1)/2} \cdot (e^{-(m+2)B} \sin((m+2)\theta) + e^{-(m+4)B} \sin(m\theta)) }
			{ 1 + 2e^{-2B} cos(2\theta) + e^{-4B} } $$
			
			<p>Putting that back into our square approximation, we get (where m is odd)</p>
	
			$$ \text{triangle}_m(\theta) = \frac{2}{\pi} \sum_{k=1}^{5} A_k \cdot \frac{ (e^{-B_k}- e^{-3B_k}) \sin(\theta) - (-1)^{(m-1)/2} \cdot (e^{-(m+2)B_k} \sin((m+2)\theta) + e^{-(m+4)B_k} \sin(m\theta)) }
			{ 1 + 2e^{-2B_k} cos(2\theta) + e^{-4B_k} } $$
			
			<p>Here is a plot of that approximation at n=15</p>
		
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{
				let A=[7.696628911325569,0.9594497374687989,0.14181977912419547,0.016299642072186815,0.0010188018664029279];
				let B=[2.672325718313832,0.9970754544305241,0.3629940341223481,0.1108529795869248,0.0225846793201704];
				let m=15;
				(A[0]*(( (exp(-B[0])-exp(-3*B[0]))*sin(x) - exp(-(m+4)*B[0])*sin((m)*x) - exp(-(m+2)*B[0])*sin((m+2)*x) ) / ( 1 + 2*exp(-2*B[0])*cos(2*x) + exp(-4*B[0]) ))
				+A[1]*(( (exp(-B[1])-exp(-3*B[1]))*sin(x) - exp(-(m+4)*B[1])*sin((m)*x) - exp(-(m+2)*B[1])*sin((m+2)*x) ) / ( 1 + 2*exp(-2*B[1])*cos(2*x) + exp(-4*B[1]) ))
				+A[2]*(( (exp(-B[2])-exp(-3*B[2]))*sin(x) - exp(-(m+4)*B[2])*sin((m)*x) - exp(-(m+2)*B[2])*sin((m+2)*x) ) / ( 1 + 2*exp(-2*B[2])*cos(2*x) + exp(-4*B[2]) ))
				+A[3]*(( (exp(-B[3])-exp(-3*B[3]))*sin(x) - exp(-(m+4)*B[3])*sin((m)*x) - exp(-(m+2)*B[3])*sin((m+2)*x) ) / ( 1 + 2*exp(-2*B[3])*cos(2*x) + exp(-4*B[3]) ))
				+A[4]*(( (exp(-B[4])-exp(-3*B[4]))*sin(x) - exp(-(m+4)*B[4])*sin((m)*x) - exp(-(m+2)*B[4])*sin((m+2)*x) ) / ( 1 + 2*exp(-2*B[4])*cos(2*x) + exp(-4*B[4]) ))
				)*4/pi/pi;}"></div>
	
			<p>For comparison, here is the exact triangle wave at n=15</p>
		
			<div class="graph" style="text-align: center;" data-xmin="-10" data-xmax="10" data-ymax="1" data-ymin="-1"
			data-function="{y=0;for(var jj=1;jj!=17;jj+=2){y+=4*sin(jj*pi/2)*sin(jj*x)/(jj*jj*pi*pi)};y}"></div>

		</div>
		<div style="height:50px"></div>
	</div>
	</body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.6.0/contrib/auto-render.min.js" integrity="sha384-v0hOo8Okju1/6E2iC8uuVf29cmfGZBHsPKw2LVlfjvJ08HnN2aj0P3/lWNKtzBWK" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.6.0/katex.min.js"></script>
	<script src="labjs/graph.js"></script>
	<script>
	renderGraphsInElement(document.body);
    renderMathInElement(document.body);
	</script>
</html>
