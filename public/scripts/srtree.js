

class search_context
{
    constructor()
    {
        this.minD = 9e9;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.val = undefined;
    }

    toString()
    {
        return this.val+" @ "+this.x+","+this.y+","+this.z
    }
}

// hardcoded dims for now
// "static r-tree"
class srtree3
{
    constructor(x0, x1, y0, y1, z0, z1)
    {
        this.leaf = (x1-x0 <= 1 && y1-y0 <= 1 && z1-z0 <= 1);
        this.x0 = x0;
        this.y0 = y0;
        this.z0 = z0;
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        if(!this.leaf)
        {
            this.sub = Array(8);
            this.tmpMin = new Float32Array(8);
            this.tmpSort = new Int32Array(8);
            for(var i=0 ; i<8 ; i++)
                this.tmpSort[i] = i;
        }
        this.count = 0;
    }

    insert(x, y, z, val)
    {
        if(x<this.x0||x>this.x1||y<this.y0||y>this.y1||z<this.z0||z>this.z1)
            throw 'poop';
        this.count ++;
        if(this.leaf)
        {
            if(this.count > 1)
                console.log("duplicate lost "+val+" @ "+x+","+y+","+z);
            this.val = val; 
        }
        else
        {
            var xi = (x-this.x0)*2/(this.x1-this.x0) | 0;
            var yi = (y-this.y0)*2/(this.y1-this.y0) | 0;
            var zi = (z-this.z0)*2/(this.z1-this.z0) | 0;
            var si = xi*4 + yi*2 + zi;
            if(!this.sub[si])
            {
                var xv = [this.x0, (this.x0+this.x1)/2, this.x1];
                var yv = [this.y0, (this.y0+this.y1)/2, this.y1];
                var zv = [this.z0, (this.z0+this.z1)/2, this.z1];
                this.sub[si] = new srtree3(xv[xi], xv[xi+1], yv[yi], yv[yi+1], zv[zi], zv[zi+1])
            }
            this.sub[si].insert(x, y, z, val);
        }
    }

    remove(x, y, z)
    {
        if(x<this.x0||x>this.x1||y<this.y0||y>this.y1||z<this.z0||z>this.z1)
            throw 'poop';
        if(this.count <= 0) 
            return 0;

        if(this.leaf)
        {
            this.count --;
            return 1;
        }
        else
        {
            var xi = (x-this.x0)*2/(this.x1-this.x0) | 0;
            var yi = (y-this.y0)*2/(this.y1-this.y0) | 0;
            var zi = (z-this.z0)*2/(this.z1-this.z0) | 0;
            var si = xi*4 + yi*2 + zi;

            if(this.sub[si] && this.sub[si].remove(x,y,z) > 0)
            {
                this.count --;
                return 1;
            }
        }
        return 0;
    }

    // smallest sphere at xyz tangent to box
    minD(x, y, z)
    {
        var minx = Math.max(0, Math.max(this.x0-x, x-this.x1));
        var miny = Math.max(0, Math.max(this.y0-y, y-this.y1));
        var minz = Math.max(0, Math.max(this.z0-z, z-this.z1));
        return minx*minx + miny*miny + minz*minz;
    }

    // smallest sphere at xyz containing box
    maxD(x, y, z)
    {
        var minx = Math.max(x-this.x0, this.x1-x);
        var miny = Math.max(y-this.y0, this.y1-y);
        var minz = Math.max(z-this.z0, this.z1-z);
        return minx*minx + miny*miny + minz*minz;
    }

    nearest(x, y, z, search)
    {
        search = search || new search_context();
        
        if(this.count == 0)
        {
        }
        else if(this.leaf)
        {
            var d = this.minD(x,y,z);
            if(d < search.minD)
            {
                search.minD = d;
                search.x = this.x0;
                search.y = this.y0;
                search.z = this.z0;
                search.val = this.val;
            }
        }
        else
        {
            for(var i=0 ; i<8 ; i++)
            {
                this.tmpMin[i] = this.sub[i] ? this.sub[i].minD(x, y, z) : 9e9;
            }
            var thiz = this;
            this.tmpSort.sort(function(a,b) { return thiz.tmpMin[a] - thiz.tmpMin[b]; });
            for(var j=0 ; j<8 ; j++)
            {
                var i = this.tmpSort[j];
                if(this.tmpMin[i] < search.minD)
                    this.sub[i].nearest(x, y, z, search);
            }
        }

        return search;
    }
}



function srtree_sanity_check()
{
    var tree = new srtree3(0,256,0,256,0,256);
    
    for(var i=0 ; i<10000 ; i++)
    {
        var x = Math.random() * 256 | 0;
        var y = Math.random() * 256 | 0;
        var z = Math.random() * 256 | 0;
        tree.insert(x,y,z,i);
    }
    console.log(tree.count);

    for(var i=0 ; i<10000 ; i++)
    {
        var x = Math.random() * 256 | 0;
        var y = Math.random() * 256 | 0;
        var z = Math.random() * 256 | 0;
        tree.remove(x,y,z);
    }

    console.log(tree.count);

    for(var i=0 ; i<20 ; i++)
    {
        var x = Math.random() * 256 | 0;
        var y = Math.random() * 256 | 0;
        var z = Math.random() * 256 | 0;

        console.log(x+","+y+","+z+" -> "+tree.nearest(x,y,z));
    }
}
