
    function hilbert3(color, m, s, x, y, z, dx, dy, dz, dx2, dy2, dz2, dx3, dy3, dz3)
    {
        if(s==1)
        {
            color[m] = x<<16|y<<8|z;
            return m+1;
        }
        else
        {
            s/=2;
            if(dx<0) x-=s*dx;
            if(dy<0) y-=s*dy;
            if(dz<0) z-=s*dz;
            if(dx2<0) x-=s*dx2;
            if(dy2<0) y-=s*dy2;
            if(dz2<0) z-=s*dz2;
            if(dx3<0) x-=s*dx3;
            if(dy3<0) y-=s*dy3;
            if(dz3<0) z-=s*dz3;
            m=hilbert3(color, m, s, x, y, z, dx2, dy2, dz2, dx3, dy3, dz3, dx, dy, dz);
            m=hilbert3(color, m, s, x+s*dx, y+s*dy, z+s*dz, dx3, dy3, dz3, dx, dy, dz, dx2, dy2, dz2);
            m=hilbert3(color, m, s, x+s*dx+s*dx2, y+s*dy+s*dy2, z+s*dz+s*dz2, dx3, dy3, dz3, dx, dy, dz, dx2, dy2, dz2);
            m=hilbert3(color, m, s, x+s*dx2, y+s*dy2, z+s*dz2, -dx, -dy, -dz, -dx2, -dy2, -dz2, dx3, dy3, dz3);
            m=hilbert3(color, m, s, x+s*dx2+s*dx3, y+s*dy2+s*dy3, z+s*dz2+s*dz3, -dx, -dy, -dz, -dx2, -dy2, -dz2, dx3, dy3, dz3);
            m=hilbert3(color, m, s, x+s*dx+s*dx2+s*dx3, y+s*dy+s*dy2+s*dy3, z+s*dz+s*dz2+s*dz3, -dx3, -dy3, -dz3, dx, dy, dz, -dx2, -dy2, -dz2);
            m=hilbert3(color, m, s, x+s*dx+s*dx3, y+s*dy+s*dy3, z+s*dz+s*dz3, -dx3, -dy3, -dz3, dx, dy, dz, -dx2, -dy2, -dz2);
            m=hilbert3(color, m, s, x+s*dx3, y+s*dy3, z+s*dz3, dx2, dy2, dz2, -dx3, -dy3, -dz3, -dx, -dy, -dz);
            return m;
        }
    }
    function colordiff6(c1, c2)
    {
        var r = (c1>>10 & 0xfc) - (c2>>10 & 0xfc)
        var g = (c1>>4  & 0xfc) - (c2>>4  & 0xfc)
        var b = (c1<<2  & 0xfc) - (c2<<2  & 0xfc)
        return r*r+g*g+b*b;
    }
    function colordiff7(c1, c2)
    {
        var r = (c1>>13 & 0xfe) - (c2>>13 & 0xfe)
        var g = (c1>>6  & 0xfe) - (c2>>6  & 0xfe)
        var b = (c1<<1  & 0xfe) - (c2<<1  & 0xfe)
        return r*r+g*g+b*b;
    }
    function colordiff8(c1, c2)
    {
        var r = (c1>>16 & 0xff) - (c2>>16 & 0xff)
        var g = (c1>>8  & 0xff) - (c2>>8  & 0xff)
        var b = (c1>>0  & 0xff) - (c2>>0  & 0xff)
        return r*r+g*g+b*b;
    }
    function hue6(c1)
    {
        var r=(c1>>10 & 0xfc);
        var g=(c1>>4  & 0xfc);
        var b=(c1<<2  & 0xfc);
        return Math.atan2(-1.732*(g-b),-2*r+g+b)/(2*Math.PI);
    }
    function hue7(c1)
    {
        var r=(c1>>13 & 0xfe);
        var g=(c1>>6  & 0xfe);
        var b=(c1<<1  & 0xfe);
        return Math.atan2(-1.732*(g-b),-2*r+g+b)/(2*Math.PI);
    }
    function hue8(c1)
    {
        var r=(c1>>16 & 0xff);
        var g=(c1>>8  & 0xff);
        var b=(c1>>0  & 0xff);
        return Math.atan2(-1.732*(g-b),-2*r+g+b)/(2*Math.PI);
    }
    function drawHilbert(canvas,v)
    {
        seed = v*rscale;
        var w = canvas.width;
        var h = canvas.height;
        var l = w*h;
        var bl = l==1<<18 ? 18 : l==1<<21 ? 21 : l==1<<24 ? 24 : 0;
        var bc = Math.floor(bl / 3);
        var bw = w==512 ? 9 : w==2048 ? 11 : w==4096 ? 12 : 0;
        var cmask = ((1<<bc)-1)<<(8-bc);
        if(bc == 0 || bw == 0) throw "poop";
        
        var context = canvas.getContext('2d');
        var img = context.createImageData(w, h);
        var colorBuffer = new ArrayBuffer(4*l);
        var color = new Int32Array(colorBuffer);
        var m=0;
        
        var o = [0x111, 0x10a, 0x08c, 0x0a1, 0x054, 0x062];
        o = o[Math.floor(v*6)];
        o = [o>>8&1,o>>7&1,o>>6&1,o>>5&1,o>>4&1,o>>3&1,o>>2&1,o>>1&1,o>>0&1];
        var x = rand();
        for(var i=0 ; i<9 ; i++) o[i] = (x>>i&1 ? o[i] : -o[i]);
        hilbert3(color,0,1<<bc,0,0,0,o[0],o[1],o[2],o[3],o[4],o[5],o[6],o[7],o[8]);
        
        function hilbert2(s, x, y, dx, dy, dx2, dy2)
        {
            if(s==1)
            {
                var xy = y<<bw | x;
                img.data[xy*4+0] = color[m]>>(bc+8) & cmask;
                img.data[xy*4+1] = color[m]>>(bc+0) & cmask;
                img.data[xy*4+2] = color[m]<<(8-bc) & cmask;
                img.data[xy*4+3] = 255;
                m++;
            }
            else
            {
                s/=2
                if(dx<0) x-=s*dx;
                if(dy<0) y-=s*dy;
                if(dx2<0) x-=s*dx2;
                if(dy2<0) y-=s*dy2;
                hilbert2(s, x, y, dx2, dy2, dx, dy);
                hilbert2(s, x+s*dx, y+s*dy, dx, dy, dx2, dy2);
                hilbert2(s, x+s*dx+s*dx2, y+s*dy+s*dy2, dx, dy, dx2, dy2);
                hilbert2(s, x+s*dx2, y+s*dy2, -dx2, -dy2, -dx, -dy);
            }
        }
        m=0;
        hilbert2(h,0,0,0,1,1,0);
        if(h<w) hilbert2(h,h,0,0,1,1,0);
        
        context.putImageData(img, 0, 0);
    }
    var seed=11111111;
    var rscale=(1<<16)*(1<<16);
    function rand()
    {
        var div = (1<<16);
        seed = (seed * 25214903917 + 11) % (div*rscale);
        //seed = (seed * 0x5DEECE66D + 11) % (div*rscale);
        
        return seed / div;
    }
    var drawPaintLock;
    function drawPaint(canvas,dirs,sort,strength,v,n)
    {
        var myLock = Math.random();
        drawPaintLock = myLock;
        seed = v*rscale;
        
        var w = canvas.width;
        var h = canvas.height;
        var l = w*h;
        var bl = l==1<<18 ? 18 : l==1<<21 ? 21 : l==1<<24 ? 24 : 0;
        var bc = bl/3;
        var bw = w==512 ? 9 : w==2048 ? 11 : w==4096 ? 12 : 0;
        var mask = l-1;
        var wmask = w-1;
        var cmask = ((1<<bc)-1)<<(8-bc);
        if(bc == 0 || bw == 0) throw "poop";
        
        var context = canvas.getContext('2d');
        var imgBytes = context.createImageData(1, 1);
        var colorBuffer = new ArrayBuffer(4*l);
        var color = new Int32Array(colorBuffer);
        var imgBuffer = new ArrayBuffer(4*l);
        var img = new Int32Array(imgBuffer);
        var bFieldBuffer = new ArrayBuffer(l);
        var bField = new Int8Array(bFieldBuffer);
        var bListBuffer = new ArrayBuffer(4*l);
        var bList = new Int32Array(bListBuffer);
        var bHead = -1;
        var hue = bc==6 ? hue6 : bc==7 ? hue7 : hue8;
        var colordiff = bc==6 ? colordiff6 : bc==7 ? colordiff7 : colordiff8;
        
        if(sort==0)
        {
            for(var i=1;i<l;i++)
            {
                var i2=Math.floor(rand()/rscale*(i+1));
                color[i]=color[i2];
                color[i2]=i;
            }
        }
        else if(sort==1)
        {
            color=[];
            for(var i=0;i<l;i++)
            {
                color[i]=i;
            }
            color=color.sort(function(a,b){return (b&((1<<bc)-1)) - (a&((1<<bc)-1));});
        }
        else if(sort == 2)
        {
            var hueBuffer=new ArrayBuffer(l);
            var huec=new Int8Array(hueBuffer);
            for(var i=0;i<l;i++)
            {
                huec[i]=((hue(i)+v)*256)&255;
            }
            color=[];
            for(var i=0;i<l;i++)
            {
                color[i]=i;
            }
            color=color.sort(function(a,b){return huec[b]-huec[a];});
        }
        else// if(sort == 3)
        {
            var o = [0x111, 0x10a, 0x08c, 0x0a1, 0x054, 0x062];
            o = o[Math.floor(v*6)];
            o = [o>>8&1,o>>7&1,o>>6&1,o>>5&1,o>>4&1,o>>3&1,o>>2&1,o>>1&1,o>>0&1];
            var r = rand();
            for(var i=0 ; i<9 ; i++) o[i] = (r>>i&1 ? o[i] : -o[i]);
            hilbert3(color,0,1<<bc,0,0,0,o[0],o[1],o[2],o[3],o[4],o[5],o[6],o[7],o[8]);
            for(var i=0 ; i<l ; i++)
            {
                color[i] = (color[i]&0xff) | (color[i]&0xff00)>>(8-bc) | (color[i]&0xff0000)>>2*(8-bc);
            }
        }
        var xyds = [1,1-w,-w,-1-w,-1,-1+w,+w,1+w];
        var xyd = [];
        for(var i=0 ; i<xyds.length ; i++)
        {
            if(dirs>>i & 1) xyd.push(xyds[i]);
        }
        var ci = l-1;
        var scan = 0;
        var mins = 9e9;
        var minxy = -1;
        
        if(!(n>0)) n = rand()/rscale*10+1;
        for(var i=0;i<n;i++)
        {
            while(img[minxy] != 0)
            {
                minxy = (rand()>>14) & mask;
                console.log(minxy % 16)
            }
            drop();
        }
        
        stride();
        function drop()
        {
            var xy = minxy;
            if(xy<0 || ci<0) return 0;
            if(bField[xy] == 2 || img[xy] != 0)
                throw 'poop';
            bField[xy]=2;
            img[xy] = color[ci];
            imgBytes.data[0] = img[xy]>>(3*bc-8) & cmask;
            imgBytes.data[1] = img[xy]>>(2*bc-8) & cmask;
            imgBytes.data[2] = img[xy]<<(8-bc)  & cmask;
            imgBytes.data[3] = 255;
            context.putImageData(imgBytes, xy&wmask, xy>>bw);
            for(var i=0 ; i<xyd.length ; i++)
            {
                var xybi = (xy+xyd[i])&mask;
                if(bField[xybi]==0)
                {
                    bField[xybi]=1;
                    bList[xybi] = bHead;
                    bHead = xybi;
                }
            }
            minxy = -1;
            mins = 9e9;
            ci--;
            return 1;
        }
        function step()
        {
            var c = 0;
            for(var xy=bHead ; xy>=0 ; xy=bList[xy])
            {
                if(bField[xy] != 1) continue;
                var s = 8e9;
                for(var i=0 ; i<xyds.length ; i++)
                {
                    var xybi = (xy+xyds[i])&mask;
                    if(bField[xybi]==2) s = Math.min(s,colordiff(color[ci], img[xybi]));
                }
                if(s <= mins)
                {
                    minxy = xy;
                    mins = s;
                }
                if(++scan == strength)
                {
                    c += drop();
                    scan = 0;
                }
            }
            if(c==0)
            {
                c += drop();
                scan = 0;
            }
            for(var xy=bHead ; xy>=0 ; xy=bList[xy])
            {
                while(bList[xy]>=0 && bField[bList[xy]]!=1)
                    bList[xy] = bList[bList[xy]];
            }
            return c;
        }
        function stride()
        {
            if(myLock!=drawPaintLock) return;
            var sci = Math.max(ci-100,0);
            while(ci >= sci && step() > 0);
            if(ci >= 0) setTimeout(stride, 0);
        }
    }