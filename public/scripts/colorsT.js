

function makeC(r, g, b)
{
    return (r&255)<<16 | (g&255)<<8 | (b&255);
}

function diffC(c1, c2)
{
    var r = (c1>>16 & 0xff) - (c2>>16 & 0xff)
    var g = (c1>>8  & 0xff) - (c2>>8  & 0xff)
    var b = (c1>>0  & 0xff) - (c2>>0  & 0xff)
    return r*r + g*g + b*b;
    // return Math.abs(r)+Math.abs(g)+Math.abs(b);
}

function diffRGB(r0, g0, b0, r1, g1, b1)
{
    var r = r0 - r1;
    var g = g0 - g1;
    var b = b0 - b1;
    return r*r + g*g + b*b;
    // return Math.abs(r)+Math.abs(g)+Math.abs(b);
}

function hueC(c1)
{
    var r=(c1>>16 & 0xff);
    var g=(c1>>8  & 0xff);
    var b=(c1>>0  & 0xff);
    return (Math.atan2(-1.732*(g-b),-2*r+g+b)/(2*Math.PI) * 256)  & 255;
}

var dirs = [[-1,0],[1,0],[0,-1],[0,1],[-1,-1],[-1,1],[1,-1],[1,1]];


class paint_core_list
{
    constructor()
    {
        this.blist = new Int32Array(1<<17);
        this.bcolor = new Int32Array(1<<17);
        this.blen = 0;
        this.reflow = 256;
        this.reflowC = this.reflow;
        this.lastI = -1;
    }

    add(x, y, cr, cg, cb)
    {
        this.blist[this.blen] = x<<16 | y;
        this.bcolor[this.blen] = cr<<16 | cg<<8 | cb<<0;
        this.blen ++;

    }
    removeLastResult()
    {
        this.blist[this.lastI] = -1;

        if(this.reflowC-- == 0)
        {
            var newlen = 0;
            for(var i=0 ; i<this.blen ; i++)
            {
                if(this.blist[i] != -1)
                {
                    this.blist[newlen] = this.blist[i];
                    this.bcolor[newlen] = this.bcolor[i];
                    newlen++;
                }
            }
            this.blen = newlen;
            this.reflowC = this.reflow;
        }
    }
    best(cr, cg, cb)
    {
        if(this.blen == 0)
            throw 'poop'

        var minI = 0;
        var minD = 9e9;
        var numC = 0

        for(var i=0 ; i<this.blen ; i++)
        {
            if(this.blist[i] == -1) continue;
            var x = this.blist[i] >> 16;
            var y = this.blist[i] & 65535;
            var cr0 = (this.bcolor[i] >> 16) & 255;
            var cg0 = (this.bcolor[i] >> 8) & 255;
            var cb0 = (this.bcolor[i] >> 0) & 255;

            var d = diffRGB(cr0, cg0, cb0, cr, cg, cb);
            numC ++;
            if(d < minD)
            {
                minI = i;
                minD = d;
            }
        }
        this.lastI = minI;

        var x = this.blist[minI] >> 16;
        var y = this.blist[minI] & 65535;
        return {x:x, y:y};
    }
}


class paint_core_tree
{
    constructor()
    {
        this.tree = new srtree3(0,256,0,256,0,256);
        this.lastS = undefined;
    }
    add(x, y, cr, cg, cb)
    {
        this.tree.insert(cr, cg, cb, x<<16 | y);
    }
    removeLastResult()
    {
        var s = this.lastS;
        this.tree.remove(s.x, s.y, s.z);
    }
    best(cr, cg, cb)
    {
        var s = this.tree.nearest(cr, cg, cb);
        this.lastS = s;
        return { x : s.val>>16 , y : s.val&0xffff };
    }
}


class painter
{
    constructor(canvas) 
    {
        var l = canvas.width * canvas.height;
        var colors = new Int32Array(l);
        var ci;
        var hr = Math.random();

        for(var i=0 ; i<l ; i++)
        {
            var ci = (1<<24) * i / l | 0;
            colors[i] = ci | ((hueC(ci) + hr*256 + Math.random()*64) & 255)<<24;
        }
        colors = colors.sort();

        this.canvas = canvas;
        this.colors = colors;
        this.clast = l-1;
        this.context = canvas.getContext('2d');

        this.image = this.context.getImageData(0, 0, canvas.width, canvas.height);

        // this.core = new paint_core_list();
        this.core = new paint_core_tree();
    }

    refresh()
    {
        this.context.putImageData(this.image, 0, 0);
    }

    putNext(x, y)
    {
        if(this.clast < 0) return;
        if(!this.getA(x|0,y|0))
            this.putC(x|0, y|0, this.colors[this.clast--])
    }

    putRGB(x, y, cr, cg, cb)
    {
        var im = this.image;
        if(x<0 || x>=im.width || y<0 || y>=im.height)
            throw 'poop';
        var xy = 4*(x+(y*im.width));

        im.data[xy+0] = cr;
        im.data[xy+1] = cg;
        im.data[xy+2] = cb;
        im.data[xy+3] = 255;

        this.core.add(x, y, im.data[xy+0], im.data[xy+1], im.data[xy+2]);
    }

    putC(x, y, rgb)
    {
        var cr = rgb>>16 & 255;
        var cg = rgb>>8  & 255;
        var cb = rgb     & 255;
        this.putRGB(x, y, cr, cg, cb);
    }

    getA(x, y)
    {
        var im = this.image;
        if(x<0 || x>=im.width || y<0 || y>=im.height)
            return 255;
        var xy = 4*(x+(y*im.width));
        return im.data[xy+3];
    }

    autoNext()
    {
        if(this.clast < 0)
            return 0;

        var color = this.colors[this.clast];
        var cr = color>>16 & 255;
        var cg = color>>8  & 255;
        var cb = color     & 255;

        var p = this.core.best(cr, cg, cb);

        var put = false;
        for(var j=0 ; j<dirs.length ; j++)
        {
            var xd = p.x+dirs[j][0];
            var yd = p.y+dirs[j][1];
            if(!this.getA(xd, yd))
            {
                this.putRGB(xd, yd, cr, cg, cb);
                put = true;
                break;
            }
        }

        if(put)
            this.clast--;
        else
            this.core.removeLastResult();

        return 1;
    }

}