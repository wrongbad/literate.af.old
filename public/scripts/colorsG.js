

function makeC(r, g, b)
{
    return (r&255)<<16 | (g&255)<<8 | (b&255);
}

function diffC(c1, c2)
{
    var r = (c1>>16 & 0xff) - (c2>>16 & 0xff)
    var g = (c1>>8  & 0xff) - (c2>>8  & 0xff)
    var b = (c1>>0  & 0xff) - (c2>>0  & 0xff)
    // return r*r+g*g+b*b;
    return Math.abs(r)+Math.abs(g)+Math.abs(b);
}

function hueC(c1)
{
    var r=(c1>>16 & 0xff);
    var g=(c1>>8  & 0xff);
    var b=(c1>>0  & 0xff);
    return (Math.atan2(-1.732*(g-b),-2*r+g+b)/(2*Math.PI) * 256)  & 255;
}

var dirs = [[-1,0],[1,0],[0,-1],[0,1],[-1,-1],[-1,1],[1,-1],[1,1]];

class painter
{
    constructor(canvas,colors,clast) 
    {
        this.canvas = canvas;
        this.colors = colors;
        this.clast = clast;
        this.context = canvas.getContext('2d');
        // this.context.translate(0.5, 0.5)
        // this.pixel = this.context.createImageData(1, 1);

        this.image = this.context.getImageData(0, 0, canvas.width, canvas.height);

        this.blist = new Int32Array(1<<17);
        this.blen = 0;
    }

    refresh()
    {
        this.context.putImageData(this.image, 0, 0);
    }

    putNext(x, y)
    {
        if(this.clast < 0) return;
        this.putC(x, y, this.colors[this.clast--])
    }

    putC(x, y, color)
    {
        var im = this.image;
        if(x<0 || x>=im.width || y<0 || y>=im.height)
            throw 'poop';
        var xy = 4*(x+(y*im.width));

        im.data[xy+0] = color>>16 & 255;
        im.data[xy+1] = color>>8  & 255;
        im.data[xy+2] = color     & 255;
        im.data[xy+3] = 255;

        this.blist[this.blen++] = x<<16 | y;
    }

    getC(x, y)
    {
        var im = this.image;
        if(x<0 || x>=im.width || y<0 || y>=im.height)
            return 0xff000000;
        var xy = 4*(x+(y*im.width));

        var c    = im.data[xy+0];
        c = c<<8 | im.data[xy+1];
        c = c<<8 | im.data[xy+2];
        c = c | im.data[xy+3] << 24;
        return c;
    }

    autoNext()
    {
        if(this.clast < 0)
            return;

        if(this.blen == 0)
            throw 'poop'

        var c = this.colors[this.clast];
        var minI = 0;
        var minD = 9e9;
        var numC = 0

        for(var i=0 ; i<this.blen ; i++)
        {
            if(this.blist[i] == -1) continue;
            var x = this.blist[i] >> 16;
            var y = this.blist[i] & 65535;
            var d = diffC(this.getC(x,y), c)
            numC ++;
            if(d < minD)
            {
                minI = i;
                minD = d;
            }
        }

        var im = this.image;
        var x = this.blist[minI] >> 16;
        var y = this.blist[minI] & 65535;
        var put = false;
        for(var j=0 ; j<dirs.length ; j++)
        {

            var xd = x+dirs[j][0];
            var yd = y+dirs[j][1];
            if(!this.getC(xd, yd))
            {
                this.putC(xd, yd, c);
                put = true;
                break;
            }
        }

        if(put)
            this.clast--;
        else
            this.blist[minI] = -1;

        if(this.clast % 256 == 0)
        {
            var newlen = 0;
            for(var i=0 ; i<this.blen ; i++)
            {
                if(this.blist[i] != -1)
                    this.blist[newlen++] = this.blist[i];
            }
            this.blen = newlen;
        }
    }

}