
// poor man's jquery selector
var dom = new Proxy({},{
    get: function(obj, prop) {
        return document.getElementById(prop);
    },
});

var getclass = new Proxy({},{ 
    get: function(obj, prop) { 
        return document.getElementsByClassName(prop); 
    }, 
});

function encodeQueryData(data)
{
   let ret = [];
   for (let d in data) if(data.hasOwnProperty(d))
     ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
   return ret.join("&");
}
function decodeQueryData(string)
{
    var pairs = string.split("&");
    var params = {};
    for(i in pairs) if(i)
    {
        var pair = pairs[i].split("=");
        params[pair[0]] = (typeof pair[1] === 'undefined') ? null : pair[1];
    }
}