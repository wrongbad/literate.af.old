with (Math) var dsp = dsp || (function(){

// private:

    var dsp = {};

    var prepR,prepI,prepN=0;
    var fftR,fftI;

    // TODO optimize table down by 8x
    function cos2pi(x)
    {
        x = round(x/prepN);
        return prepR[x];
    }
    function sin2pi(x)
    {
        x = round(x/prepN);
        return prepI[x];
    }


    function fftR2(outputR, outputI, oOffset, inputR, inputI, iStep , iOffset , N)
    {
        if(N == 1)
        {
            outputR[oOffset] = inputR[iOffset];
            outputI[oOffset] = inputI[iOffset];
        }
        else if(N == 4)
        {
            outputR[oOffset+0] = inputR[iOffset+0*iStep] + inputR[iOffset+1*iStep] + inputR[iOffset+2*iStep] + inputR[iOffset+3*iStep];
            outputI[oOffset+0] = inputI[iOffset+0*iStep] + inputI[iOffset+1*iStep] + inputI[iOffset+2*iStep] + inputI[iOffset+3*iStep];
            outputR[oOffset+1] = inputR[iOffset+0*iStep] + inputI[iOffset+1*iStep] - inputR[iOffset+2*iStep] - inputI[iOffset+3*iStep];
            outputI[oOffset+1] = inputI[iOffset+0*iStep] - inputR[iOffset+1*iStep] - inputI[iOffset+2*iStep] + inputR[iOffset+3*iStep];
            outputR[oOffset+2] = inputR[iOffset+0*iStep] - inputR[iOffset+1*iStep] + inputR[iOffset+2*iStep] - inputR[iOffset+3*iStep];
            outputI[oOffset+2] = inputI[iOffset+0*iStep] - inputI[iOffset+1*iStep] + inputI[iOffset+2*iStep] - inputI[iOffset+3*iStep];
            outputR[oOffset+3] = inputR[iOffset+0*iStep] - inputI[iOffset+1*iStep] - inputR[iOffset+2*iStep] + inputI[iOffset+3*iStep];
            outputI[oOffset+3] = inputI[iOffset+0*iStep] + inputR[iOffset+1*iStep] - inputI[iOffset+2*iStep] - inputR[iOffset+3*iStep];
        }
        else
        {
            fftR2(outputR , outputI , oOffset     , inputR , inputI , iStep*2 , iOffset , N/2);
            fftR2(outputR , outputI , oOffset+N/2 , inputR , inputI , iStep*2 , iOffset+iStep , N/2);
            for(let i=0 ; i<N/2 ; i++)
            {
                let twidR = cos2pi(i/N);
                let twidI = sin2pi(i/N);
                let eR = outputR[oOffset+i];
                let eI = outputI[oOffset+i];
                let oR = outputR[oOffset+i+N/2];
                let oI = outputI[oOffset+i+N/2];
                outputR[oOffset+i] = eR + (twidR * oR - twidI * oI);
                outputI[oOffset+i] = eI + (twidR * oI + twidI * oR);
                outputR[oOffset+i+N/2] = eR - (twidR * oR - twidI * oI);
                outputI[oOffset+i+N/2] = eI - (twidR * oI + twidI * oR);
            }
        }
    }









// public:

    dsp.fftprep = function(n)
    {
        n=n*2-1;
        for(var N = 1 ; (n = n>>>1) ; N = N<<1);
        if(prepN < N)
        {
            prepR = array(N);
            prepI = array(N);
            fftR = array(N);
            fftI = array(N);
            for(let i=0 ; i<N ; i++)
            {
                prepR[i] = cos(PI*2*i/N);
                prepI[i] = sin(PI*2*i/N);
            }
            prepN = N;
        }
        return prepN;
    }

    dsp.fft = function(x)
    {
        let xR = x[0];
        let xI = x[1];
        let N = xR.length;
        dsp.fftprep(N);
    	if(N != prepN)
            return;
        fftR2(fftR, fftI, 0, xR, xI, 1, 0, N);
        return [fftR, fftI];
    }

    dsp.name = 'dsp';

    return dsp;

})();

if(lab) 
    lab.load(dsp);