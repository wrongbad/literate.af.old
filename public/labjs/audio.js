with (Math) var audio = audio || (function(){

// private:

    var audio = {};
    var buffSize = 1024
    var context = new (window.AudioContext||window.webkitAudioContext)();
    var processor;
    var callback;
    var notemap = ['z','s','x','d','c','v','g','b','h','n','j','m',',','l','.',';','/','q','2','w','3','e','4','r','t','6','y','7','u','i','9','o','0','p','-','['];
    var notedown = new Int8Array(64); 
    var state = new Float64Array(128);

    function onaudioprocess(device)
    {
        if(callback)
            callback(processor, device.outputBuffer.getChannelData(0));
    }

// public:

    audio.open = function(bufferSize = 1024) 
    {
        buffSize = bufferSize
        if(processor) processor.disconnect();
        processor = context.createScriptProcessor(buffSize,1,1);
        processor.onaudioprocess = onaudioprocess;
        processor.connect(context.destination);
        processor.keysdown = 0;
        processor.keynote = 0;
        processor.phase = 0;
    }

    audio.setCallback = function(func)
    {
        callback = func;
    }

    audio.close = function()
    {
        processor.disconnect();
    }

    audio.keyboard = function()
    {
        dom.keygrabber.show();
    }

    audio.keydown = function(e)
    {
        var n = notemap.indexOf(e.key);
            // console.log(n);
        if(n >= 0 && processor)
        {
            processor.keynote = n;
            if(notedown[n] == 0)
            {
                notedown[n] = 1;
                processor.keysdown ++;
            }
        }
    }

    audio.keyup = function(e)
    {
        var n = notemap.indexOf(e.key);
        if(n >= 0 && processor)
        {
            // var n = notemap[e.key];
            // processor.keynote = n;
            if(notedown[n] > 0)
            {
                notedown[n] = 0;
                processor.keysdown --;
            }
        }
    }

// TODO: broken abstraction layers

    document.addEventListener("keydown", function(e) {
        // console.log(e.key);

        if(dom.keygrabber.hidden == false)
        {
            if(e.key == "Escape")
            {
                dom.keygrabber.hide();
                processor.keysdown = 0;
                return;
            }
            dom.keydisplay.innerHTML = e.key;
            audio.keydown(e);

            if(!e.ctrlKey && !e.altKey && !e.metaKey)
            {
                e.preventDefault();
            }
        }

    }, false);

    document.addEventListener("keyup", function(e) {
        // console.log(e.key);

        // make it easy to release a key
        audio.keyup(e);

        // if(dom.keygrabber.hidden == false)
        // {
            // if(e.ctrlKey || e.altKey || e.metaKey)
            // {
            //     return;
            // }
        // }

    }, false);

    audio.name = 'audio';

    return audio;

})();

if(lab) 
    lab.load(audio);