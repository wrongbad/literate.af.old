

function graph(canvas)
{
	this.canvas = canvas;
	this.ymin = -1;
	this.ymax = 1;
	this.xmin = -1;
	this.xmax = 1;
	this.xlabel = "x";
	this.ylabel = "y";
	this.plotcolor = "#0000ff";
	
	this.functions = [];
	
	this.drawAxes = function()
	{
		ctx=this.canvas.getContext("2d");
		var w=ctx.canvas.width;
		var h=ctx.canvas.height;
		this.functions = [];

		ctx.fillStyle='#ffffff';
		ctx.fillRect(0,0,w,h);
		
		ctx.beginPath();
		ctx.strokeStyle = "#808080"; 

		ctx.font="12px Helvetica";
		ctx.textAlign="center";

		var xi = (w/(this.xmax-this.xmin))*(0-this.xmin);
		var yi = h - (h/(this.ymax-this.ymin))*(0-this.ymin);
		
		if(0 < xi && xi < w)
			ctx.moveTo(xi, 0),ctx.lineTo(xi, h);
		else
			ctx.moveTo(0, 0),ctx.lineTo(0, h);
		if(0 < yi && yi < h)
			ctx.moveTo(0, yi),ctx.lineTo(w, yi);
		else
			ctx.moveTo(0, 0),ctx.lineTo(w, 0);
			
		ctx.stroke();
	}
	
	this.plot = function(func)
	{
		ctx=this.canvas.getContext("2d");
		var w=ctx.canvas.width;
		var h=ctx.canvas.height;
		
		this.functions.push(func);
		
		ctx.beginPath();
		ctx.strokeStyle = this.plotcolor; 
		
		var ln = Math.log;
		var log = Math.log;
		var sin = Math.sin;
		var cos = Math.cos;
		var tan = Math.tan;
		var asin = Math.asin;
		var acos = Math.acos;
		var atan = Math.atan;
		var exp = Math.exp;
		var e = Math.E;
		var pi = Math.PI;
		var sqrt = Math.sqrt;
		
		for(var xi=0 ; xi<w ; xi++)
		{
			var x = Number(this.xmin) + (xi/w)*(this.xmax-this.xmin);
			var y = 0;
			try {
				y = eval(func);
			} catch(err) {console.log(err);break;}
			var yi = h - h*(y-this.ymin)/(this.ymax-this.ymin);
			if(xi==0)
				ctx.moveTo(xi,yi);
			else
				ctx.lineTo(xi,yi);
		}
		ctx.stroke();
	};
	
	this.addFunction = function(func)
	{
		this.functions.push(func);
	};
	
	this.redraw = function()
	{
		var funcs = this.functions;
		
		this.drawAxes();
		for(var i=0 ; i<funcs.length ; i++)
		{
			this.plot(funcs[i])	
		}
	};
	
	this.zoomInX = function()
	{
		var oxmax = this.xmax;
		var oxmin = this.xmin;
		
		this.xmax = (oxmin+oxmax)/2 + (oxmax-oxmin)/4;
		this.xmin = (oxmin+oxmax)/2 - (oxmax-oxmin)/4;
		
		this.redraw();	
	}
	this.zoomOutX = function()
	{
		var oxmax = this.xmax;
		var oxmin = this.xmin;
		
		this.xmax = (oxmin+oxmax)/2 + (oxmax-oxmin);
		this.xmin = (oxmin+oxmax)/2 - (oxmax-oxmin);
		
		this.redraw();
	}
}



function renderGraphsInElement(elem)
{    
	var ignoredTags = [
        "script", "noscript", "style", "textarea", "pre", "code",
    ];
	var params = ["xmin","xmax","ymin","ymax","xlabel","ylabel"];
	
    for (var i = 0; i < elem.childNodes.length; i++) 
	{
        var childNode = elem.childNodes[i];
        if (childNode.nodeType === 1)
		{
            if(ignoredTags.indexOf(childNode.nodeName.toLowerCase()) != -1)
				continue;
			if(childNode.className == "graph")
			{
				var data = childNode.dataset;
		        var canvas = document.createElement('canvas');
				canvas.width = data.width || 600;
				canvas.height = data.height || 400;
				childNode.appendChild(canvas);
				
				var control = document.createElement('div');
				childNode.appendChild(control);
					
				var gr = new graph(canvas);
				/*
				var b1 = document.createElement('button');
				b1.innerHTML = '&#8596; +';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
				b1.onclick = function(){gr.zoomInX()};
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8596; -';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
				b1.onclick = function(){gr.zoomOutX;};
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8597; +';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8597; -';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8592;';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8593;';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8594;';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
			
				b1 = document.createElement('button');
				b1.innerHTML = '&#8595;';
				b1.style = 'font-size : 20px; height: 30px;';
				control.appendChild(b1);
				*/
				for(var j=0 ; j<params.length ; j++)
				{
					if(params[j] in data) gr[params[j]] = data[params[j]];
				}
				
				gr.drawAxes();
				if("color" in data)
					gr.plotcolor = data.color;
				if("function" in data)
					gr.plot(data.function);
				
				for(var j=0 ; j<20 ; j++)
					if(("function"+j) in data)
					{
						if(("color"+j) in data)
							gr.plotcolor = data["color"+j];
						gr.plot(data["function"+j]);
					}
			}
			else
                renderGraphsInElement(childNode);
        }
    }
}


function numberKatexLinesInElement(elem)
{
	
}
