var lab = lab || (function () {

// private:

    var lab = {};
    var persist = null;

    // Proxy magic in 2 layers
    // Outer vars layer captures all variable read/writes
    // Inner static layer overlays read-only stuff (built in functions)
    // "with (vars) with (static_) { code }"
    // makes that happen

    var vars_ = {};
    var vars = new Proxy(vars_, { 
        get: function(obj, prop, receiver) {
            if(prop === Symbol.unscopables)
                return [];
            else
                return obj[prop];
        },
        set: function(obj, prop, val, receiver) {
            obj[prop] = val;
            if(persist)
                persistItem(prop, val);
        },
        has: function(obj, prop) {
            return !['static_','Math'].includes(prop);
        }
    });

    var static_ = {} 
    var static = new Proxy(static_, {
        set: function(obj, prop, val, receiver) {
            Object.defineProperty(obj, prop, {
                value: val,
                writable: false,
                enumerable: true,
                configurable: false
            });
        },
    });


    // Prorotype conveniences

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };




    function persistItem(prop, val)
    {
        if(persist)
        {
            if(typeof(val) == 'object' && val.constructor.name == 'Float64Array')
                persist[prop] = 'new Float64Array(['+val+'])';
            else
                persist[prop] = String(val);
        }
    }

// public:

    lab.cleanSlate = function()
    {
        for(i in vars_)
        {
            delete vars_[i];
        }
    }

    lab.persist = function(storage)
    {
        persist = storage;
        for(i in persist)
        {
            let val = persist[i];
            try { val = eval('('+val+')'); } catch(e) {}
            vars_[i] = val;
        }
    }

    lab.preprocess = function(expr)
    {
        let editrgx = /edit\s+([a-zA-Z][a-zA-Z0-9]*)/;
        let m=expr.match(editrgx);
        if(m && m[0]==expr)
            expr = expr.replace(editrgx, 'edit("$1")');

        let state=0;
        let insing=1;
        let indoub=2;
        let escape=0;
        let mathranges = [];
        let pendrange = {};

        // TODO optimize
        for(let i=0 ; i<expr.length ; i++)
        {
            if(state == 0 && expr[i] == '\'')
            {
                pendrange.start=i;
                state = insing;
                continue;
            }
            if(state == 0 && expr[i] == '\"')
            {
                state = indoub;
                continue;
            }
            if(state == indoub || state == insing)
            {
                if(escape)
                {
                    escape = 0;
                    continue;
                }
                if(expr[i] == '\\')
                {
                    escape = 1;
                    continue;
                }
            }
            if(state == indoub && expr[i] == '\"')
            {
                state = 0;
                continue;
            }
            if(state == insing && expr[i] == '\'')
            {
                pendrange.end=i+1;
                mathranges.push(pendrange);
                pendrange={};
                state = 0;
                continue;
            }
        }

        let newexpr = '';
        let read = 0;
        for(let i=0 ; i<mathranges.length ; i++)
        {
            let r = mathranges[i];
            // console.log(mathranges[i].start+":"+mathranges[i].end);
            newexpr += expr.substring(read, r.start);
            newexpr += 'math('
            newexpr += expr.substring(r.start, r.end);
            newexpr += ')';
            read = r.end;
        }
        newexpr += expr.substring(read);
        // console.log(newexpr);
        return newexpr;
    }

    lab.eval = function(expr)
    {
        try {
            expr = lab.preprocess(expr);
            expr = 'with (vars) with (Math) with (static_) {'+expr+'}'
            return eval(expr);
        } catch(e) {
            console.log(e);
            console.log("Caused By: "+expr);
            return e;
        }
    }

    lab.load = function(lib)
    {
        if(lib.name)
        {
            static[lib.name] = lib;
        }
        else for(prop in lib)
        {
            static[prop] = lib[prop];
        }
    }

    lab.getvar = function(name)
    {
        return vars[name];
    }

    lab.typeof = function(vari)
    {
        let t = typeof(vari);
        
        if(t == 'number' || t == 'boolean')
            return t;
    }

    // built-ins

    static.array = function(len)
    {
        return new Float64Array(len);
    }

    static.math = function(expr)
    {
        return math.eval(expr, vars_);
    }

    static.console = console;
    static.isNaN = isNaN;

    return lab;

})();